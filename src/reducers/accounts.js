/*-- Farmers Reducers --*/
import * as actions from '../consts/account-action-types';
import searchCompare from '../tools/searchCompare'

let add = {
  adding: false,
  added: false,
  addError: null
}
let auth = {
  user: {},
  authenticating: false,
  authenticated: false,
  authenticateError: null
}
let update = {
  updating: false,
  updated: false,
  updateError: null
};
let changePassword = {
  changingPassword: false,
  changedPassword: false,
  changePasswordError: null,
}
let disable = {
  disabling: false,
  disabled: false,
  disableError: null
}
let deleteState = {
  deleting: false,
  deleted: false,
  deleteError: null
}
let detail = {
  detail: {},
  fetchingDetail: false,
  fetchedDetail: false,
  detailError: null
}
const InitialState = {
  accounts: [],
  fetching: false,
  fetched: false,
  error: null,
  display: [],
  ...add,
  ...auth,
  ...update,
  ...deleteState,
  ...disable,
  ...changePassword,
  ...detail
};

export default (state = InitialState, action) => {
  switch (action.type) {
    case actions.FETCH_ACCOUNTS.initial:
      return {
        ...state,
        fetching: true
      };
      break;
    case actions.FETCH_ACCOUNTS.fulfilled:
      return {
        ...state,
        fetching: false,
        fetched: true,
        accounts: action.payload,
        display: action.payload
      };
      break;
    case actions.FETCH_ACCOUNTS.rejected:
      return {
        ...state,
        fetching: false,
        fetched: false,
        error: action.payload
      };
      break;

     // Add

    case actions.FETCH_ACCOUNT.initial:
     return {
       ...state,
       fetchingDetail: true
     };
     break;
    case actions.FETCH_ACCOUNT.fulfilled:
     return {
       ...state,
       fetchingDetail: false,
       fetchedDetail: true,
       detail: action.payload
     };
     break;
    case actions.FETCH_ACCOUNT.rejected:
     return {
       ...state,
       fetchingDetail: false,
       fetchedDetail: false,
       detailError: action.payload
     };
     break;

    // Add
    case actions.ADD_ACCOUNT.initial:
      return {
        ...state,
        adding: true
      };
      break;
    case actions.ADD_ACCOUNT.fulfilled:
      return {
        ...state,
        adding: false,
        added: true
      };
      break;
    case actions.ADD_ACCOUNT.rejected:
      return {
        ...state,
        adding: false,
        added: false,
        addError: action.payload
      };
      break;

     // Reset

    // Update
    case actions.UPDATE_ACCOUNT.initial:
      return {
        ...state,
        updating: true
      };
      break;
    case actions.UPDATE_ACCOUNT.fulfilled:
      return {
        ...state,
        updating: false,
        updated: true
      };
      break;
    case actions.UPDATE_ACCOUNT.rejected:
      return {
        ...state,
        updating: false,
        updated: false,
        updateError: action.payload
      };
      break;
    //

    // Authenticate Account
    case actions.AUTH_ACCOUNT.initial:
      return {
        ...state,
        authenticating: true
      };
      break;
    case actions.AUTH_ACCOUNT.fulfilled:
      return {
        ...state,
        authenticating: false,
        authenticated: true,
        user: action.payload
      };
      break;
    case actions.AUTH_ACCOUNT.rejected:
      return {
        ...state,
        authenticating: false,
        authenticated: false,
        authenticateError: action.payload
      };
      break;
    case actions.LOGOUT:
      return {
        ...state,
        authenticating: false,
        authenticated: false
      };
      break;
    //

    // Set Account
    // case actions.SET_ACCOUNT.initial:
    //   return {
    //     ...state,
    //     authenticating: true
    //   };
    //   break;
    case actions.SET_ACCOUNT.fulfilled:
      return {
        ...state,
        // authenticating: false,
        // authenticated: true,
        user: action.payload
      };
      break;
    // case actions.SET_ACCOUNT.rejected:
    //   return {
    //     ...state,
    //     authenticating: false,
    //     authenticated: false,
    //     authenticateError: action.payload
    //   };
    //   break;
    //

    // Change Password
    case actions.CHANGE_ACCOUNT_PASSWORD.initial:
      return {
        ...state,
        changingPassword: true
      };
      break;
    case actions.CHANGE_ACCOUNT_PASSWORD.fulfilled:
      return {
        ...state,
        changingPassword: false,
        changedPassword: true
      };
      break;
    case actions.CHANGE_ACCOUNT_PASSWORD.rejected:
      return {
        ...state,
        changingPassword: false,
        changedPassword: false,
        changePasswordError: action.payload
      };
      break;
    //

    // Delete
    case actions.DELETE_ACCOUNT.initial:
      return {
        ...state,
        deleting: true
      };
      break;
    case actions.DELETE_ACCOUNT.fulfilled:
      return {
        ...state,
        deleting: false,
        deleted: true
      };
      break;
    case actions.DELETE_ACCOUNT.rejected:
      return {
        ...state,
        deleting: false,
        deleted: false,
        deleteError: action.payload
      };
      break;

     //

    // Disable
    case actions.DISABLE_ACCOUNT.initial:
      return {
        ...state,
        disabling: true
      };
      break;
    case actions.DISABLE_ACCOUNT.fulfilled:
      return {
        ...state,
        disabling: false,
        disabled: true
      };
      break;
    case actions.DISABLE_ACCOUNT.rejected:
      return {
        ...state,
        disabling: false,
        disabled: false,
        disableError: action.payload
      };
      break;

     // Reset

    // General
    case actions.SEARCH_ACCOUNT:
      return {
        ...state,
        display: state.accounts.filter((account) =>
        searchCompare(action.payload, account.firstName) ||
        searchCompare(action.payload, account.lastName))
      };
      break;
    case actions.RESET_ACCOUNT:
      return {
        ...state,
        ...add,
        ...update,
        ...deleteState
      };
      break;
    default:
      return state
  }
}
