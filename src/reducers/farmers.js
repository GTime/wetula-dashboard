/*-- Farmers Reducers --*/
import * as actions from '../consts/farmer-action-types';
import searchCompare from '../tools/searchCompare'


let detail = {
  detail: {},
  fetchingDetail: false,
  fetchedDetail: false,
  detailError: null
}
const InitialState = {
  farmers: [],
  fetching: false,
  fetched: false,
  error: null,
  display: [],
  ...detail
};

export default (state = InitialState, action) => {
  switch (action.type) {
    case actions.FETCH_FARMERS.initial:
      return {
        ...state,
        fetching: true
      };
      break;
    case actions.FETCH_FARMERS.fulfilled:
      return {
        ...state,
        fetching: false,
        fetched: true,
        farmers: action.payload,
        display: action.payload
      };
      break;
    case actions.FETCH_FARMERS.rejected:
      return {
        ...state,
        fetching: false,
        fetched: false,
        error: action.payload
      };
      break;

     // Add

    case actions.FETCH_FARMER.initial:
     return {
       ...state,
       fetchingDetail: true
     };
     break;
    case actions.FETCH_FARMER.fulfilled:
     return {
       ...state,
       fetchingDetail: false,
       fetchedDetail: true,
       detail: action.payload
     };
     break;
    case actions.FETCH_FARMER.rejected:
     return {
       ...state,
       fetchingDetail: false,
       fetchedDetail: false,
       detailError: action.payload
     };
     break;

    // General
    case actions.SEARCH_FARMER:
      return {
        ...state,
        display: state.farmers.filter((farmer) =>
        searchCompare(action.payload, farmer.firstName) ||
        searchCompare(action.payload, farmer.lastName))
      };
      break;
    default:
      return state
  }
}
