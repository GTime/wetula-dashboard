import * as actions from '../consts/scale-measurement-action-types';
import searchCompare from '../tools/searchCompare'

const InitialState = {
  scaleMeasurements: [],
  fetching: false,
  fetched: false,
  error: null,
  display: []
};

export default (state = InitialState, action) => {
  switch (action.type) {
    case actions.FETCH_SCALE_MEASUREMENTS.initial:
      return {
        ...state,
        fetching: true
      };
      break;
    case actions.FETCH_SCALE_MEASUREMENTS.fulfilled:
      return {
        ...state,
        fetching: false,
        fetched: true,
        scaleMeasurements: action.payload,
        display: action.payload
      };
      break;
    case actions.FETCH_SCALE_MEASUREMENTS.rejected:
      return {
        ...state,
        fetching: false,
        fetched: false,
        error: action.payload
      };
      break;

    // General
    case actions.SEARCH_SCALE_MEASUREMENT:
      return {
        ...state,
        display: state.scaleMeasurements.filter((scaleMeasurement) =>
        searchCompare(action.payload, scaleMeasurement.firstName) ||
        searchCompare(action.payload, scaleMeasurement.lastName))
      };
      break;
    default:
      return state
  }
}
