import { combineReducers } from 'redux'
import farmers from './farmers';
import accounts from './accounts';
import scaleMeasurements from './scaleMeasurements';
export default combineReducers({
  farmers,
  accounts,
  scaleMeasurements
})
