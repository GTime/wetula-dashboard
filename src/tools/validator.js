/* *
* validator
*
* */



// Check if empty
const isEmpty = (item) => {
	return(
		item === null ||
		item === undefined ||
		(item.hasOwnProperty('length') && item.length === 0) ||
		(item.constructor === Object && Object.keys(item).length === 0)
	)
}

// Check length
const isLength = (length, type, item) => {
	switch (type) {
		case 'GREATER': {
			return(item.length > length)
		}
		case 'LESSER': {
			return(item.length < length)
		}
		case 'EQUAL': {
			return(item.length === length)
		}
		default:

	}

}

// Check Equality
const equals = (item1, item2) => {
	return(item1 === item2)
}

export default {
	isEmpty,
	isLength,
	equals
};
