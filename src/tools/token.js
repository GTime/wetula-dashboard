/* *
* AuthService
* Description:
* This contains function that helps with Authenification services like:
* - Login
* - Logout
*
* */

export function getToken(name){
  return localStorage.getItem(name)
}
export function setToken(name, token){
  return localStorage.setItem(name, token)
}
