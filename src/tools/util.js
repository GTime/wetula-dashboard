// Trimming Description
export const formatDesc = (string='', limit=0) =>{
	if(string.length > limit){
		return string.substring(0, limit).concat('...');
	}else {
		return string;
	}
}

//
const isAllowed = (roles, allowed) => {
  if (isEmpty(roles) || isEmpty(allowed)) {
    return false;
  }
  const isAllowed = roles.find(role => allowed.includes(role.name));
  return !isEmpty(isAllowed)? true : false;
}

//Formatting Background Image
export const formatBackground= (image, height, size = "cover") =>{
	return {
		height,
		backgroundImage: "url('"+ image +"')",
		backgroundRepeat: "no-repeat",
		backgroundPosition: "center center",
		backgroundSize: size
	}
}

// Date format
export const formatDate = (date) => {
  const stringMonth = ["Jan",  "Feb",  "Mar", "Apr", "May", "Jun",  "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
  const getDate = new Date(date);
  const month = stringMonth[getDate.getMonth()];
  return {
    date: getDate.getDate(),
    month: month,
		year: getDate.getFullYear()
  }
}

// Limiter: get an array and pick out a limited number of items in the array
export const limiter = (array = [], limit) => {
	let limited = [];
	// Check array length and limit
	if(array.length < limit) {
		limit = array.length;
	}
	// Limitting......
	for(var i = 0; i < limit; i ++){
		limited.push(array[i]);
	}
	return limited;
}

// Check if empty
export const isEmpty = (item) => {
	return(
		item === null ||
		item === undefined ||
		(item.hasOwnProperty('length') && item.length === 0) ||
		(item.constructor === Object && Object.keys(item).length === 0)
	);
}

// Get One
export const getOne = (id, items) => {
	return items.find((item) => id === item.id)
}

// Get One
export const getOneSpecified = (what, where, items) => {
	let selectedItem = null;
	items.forEach((item) => {
		if(what === item[where]){
			selectedItem =  item;
		}
	})
	return selectedItem;
}
