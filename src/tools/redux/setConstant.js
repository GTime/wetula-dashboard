/* -- -- */

export default action => ({
  initial: action.toUpperCase(),
  fulfilled: action + '_fulfilled'.toUpperCase(),
  rejected: action + '_rejected'.toUpperCase()
})
