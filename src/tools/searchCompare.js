

export default (what, where) => {
  const searchWord = what.toLowerCase();
  const searchPlace = where.toLowerCase();
  return searchPlace.includes(searchWord)
}
