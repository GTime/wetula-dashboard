import { isEmpty } from '../tools/util';

export default (data) => {
  let errors = {}

  if(isEmpty(data.userName)){
    errors.userName = "User name is required"
  }
  if(isEmpty(data.type)){
    errors.type = "Account type is required"
  }
  if(isEmpty(data.password)){
    errors.password = "Password is required"
  }
  if(data.password !== data.verifyPassword){
    errors.verifyPassword = "Did not match password"
  }

  return{
    errors,
    isValid: isEmpty(errors)
  }
}
