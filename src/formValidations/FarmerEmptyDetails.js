import { isEmpty } from '../tools/util';

export default (data) => {
  let errors = {}

  if(isEmpty(data['details.firstName'])){
    errors['details.firstName'] = "First Name is required";
  }
  if(isEmpty(data['details.lastName'])){
    errors['details.lastName'] = "Last Name is required";
  }
  if(isEmpty(data['details.dob'])){
    errors['details.dob'] = "Date of birth is required";
  }
  if(isEmpty(data['details.gender'])){
    errors['details.gender'] = "Gender is required";
  }
  if(isEmpty(data['details.phone']) ||  data['details.phone'].length < 10){
    errors['details.phone'] = "Phone Number  is required and should of 10 characters";
  }
  if(isEmpty(data['details.email']) ){
    errors['details.email'] = "Email is required";
  }

  return{
    errors,
    isValid: isEmpty(errors)
  }
}
