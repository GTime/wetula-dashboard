import Validator from '../tools/validator'
import { isEmpty } from '../tools/utilities'

export default (data) => {
  let errors = {}

  if(Validator.isEmpty(data.firstName)){
    errors.firstName = "First Name is required"
  }
  if(Validator.isEmpty(data.lastName)){
    errors.lastName = "Last Name is required"
  }
  if(Validator.isEmpty(data.gender)){
    errors.gender = "Gender is required"
  }
  if(Validator.isEmpty(data.email)){
    errors.email = "Email is required"
  }
  if(Validator.isEmpty(data.role)){
    errors.role = "Role is required"
  }
  if(Validator.isEmpty(data.phoneNumber) ||  !Validator.isLength(10, 'EQUAL', data.phoneNumber)){
    errors.phoneNumber = "Phone Number  is required and should of 10 characters"
  }


  return{
    errors,
    isValid: isEmpty(errors)
  }
}
