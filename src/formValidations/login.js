import { isEmpty } from '../tools/util';

export default (data) => {
  let errors = {}

  if(isEmpty(data.userName)){
    errors.userName = "User name is required"
  }
  if(isEmpty(data.password)){
    errors.password = "Password is required"
  }
  return{
    errors,
    isValid: isEmpty(errors)
  }
}
