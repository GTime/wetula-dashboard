import React, { Component } from 'react';
import { connect } from 'react-redux';
import { isEmpty } from '../tools/util';
import withDashboard from './HOC/withDashboard';
import { fetchAccount, updateAccount, resetAccount } from '../actions/accounts';

import Preloader from './presentationals/extra/LinearPreloader';
import DeleteSuccess from  './presentationals/extra/DeleteSuccess';
import SwitchView from  './presentationals/extra/SwitchView';
import Header from  './presentationals/accounts/Header';
import Details from  './presentationals/accounts/Details';
import Settings from  './presentationals/accounts/Settings';

class Detail extends Component {
	constructor(props){
		super(props);
		this.state = {
			activeLink: 'details'
		};
		this.changeView = this.changeView.bind(this);
	}
	componentWillMount(){
		this.props.fetchAccount(this.props.match.params.id);
	}
	componentWillUpdate(nextProps){
    if(nextProps.update.updated == true &&  nextProps.fetchedDetail == true){
			console.log('Updated', nextProps.update.updated);
      this.props.fetchAccount(this.props.match.params.id);
    }
  }
	changeView(e){
		const { link } = e.target.dataset;
		if (link) {
			this.setState({ activeLink: link})
		}
	}
	render(){
		const {
			fetching,
			fetched,
			error,
			detail,
			deleteState,
			update,
			updateAccount,
			reset
		} = this.props;
		let headerProps = {
			nav:{
				activeLink: this.state.activeLink,
				links: [
					{title: 'Details', link: 'details'},
					{title: 'Settings', link: 'settings'}
				],
				changeView: this.changeView
			},
			detail: detail
		}
		let switchView = {
			activeLink: this.state.activeLink,
			views: [
				{
					link: 'details',
					component: Details,
					props: detail
				},
				{
					link: 'settings',
					component: Settings,
					props: {
						detail,
						update,
						updateAccount
					}
				}
			]
		}

		if (deleteState.deleted) {
			return <DeleteSuccess link='/farmers' title="Farmers"/>
		}
		return (
			<main className="main-container">
				{
					(fetched === true)?
					<Header {...headerProps}/> :
					null
				}
				<div className="main-content">
					 <Preloader active={fetching}/>
					 {
						 (fetched === true) && !isEmpty(detail)?
						 <SwitchView {...switchView}/> :
						 null
					 }
					 {
						 ((fetched === true) && isEmpty(detail))?
						 <div>
							 <h1>Not Found</h1>
							 <a href='/farmers' className='btn'>Go to Farmers</a>
						 </div> :
						 null
					 }
					 {
						 (error === true)?
						 <h1>An error occured</h1> :
						 null
					 }
				</div>
				{this.props.footer}
			</main>
		)
	}
}
const mapStateToProps = ({ accounts }) => {
	return {
		detail: accounts.detail,
		fetching: accounts.fetchingDetail,
		fetched: accounts.fetchedDetail,
		error: accounts.detailError,
		deleteState: {
			deleting: accounts.deleting,
			deleted: accounts.deleted,
			error:accounts.deleteError
		},
		update: {
			updating: accounts.updating,
			updated: accounts.updated,
			error:accounts.updateError
		}
	}
}
const mapDispatchToProps = (dispatch) => ({
	fetchAccount: (id) => dispatch(fetchAccount(id)),
	updateAccount: (id, data) => dispatch(updateAccount(id, data)),
	reset: () => dispatch(resetAccount())
})

const MappedComponent = connect(mapStateToProps, mapDispatchToProps)(Detail);
export default withDashboard(MappedComponent);
