
import React, { Component } from 'react';
import { isEmpty } from '../../tools/util';


export default (Wrapper) => {
  return class extends Component {
    constructor(props) {
      super(props);
      this.state = {
        enableUpdate: false
      }
      this.handleChange = this.handleChange.bind(this)
      this.handleSubmit = this.handleSubmit.bind(this)
    }
    clean(){
      for (var key in this.state) {
        if (key == 'enableUpdate'){
          delete this.state[key]
        }
      }
    }
    clearForm(){
      this.setState({ enableUpdate: false })
      this.props.actions.reset();
    }
  	handleChange(e) {
      if (!isEmpty(e.target.value)) {
        this.setState({ enableUpdate: true })
      }
  		this.setState({ [e.target.name]:  e.target.value });
      this.clean();
  	}
  	handleSubmit(e) {
  		e.preventDefault();
      const { detail, updateAction } = this.props;
      this.clean();
      if (!isEmpty(this.state)) {
        updateAction(detail._id, this.state)
      }
  	}
    render () {
      const formProps = {
        enableUpdate: this.state.enableUpdate,
        handleChange: this.handleChange,
        handleSubmit: this.handleSubmit
      }

      return <Wrapper form={formProps} {...this.props} />
    }
  }
}
