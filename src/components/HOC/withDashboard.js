import React, { Component } from 'react';
import { connect } from 'react-redux';
import { logout } from '../../actions/accounts';

import Header from '../presentationals/layout/header/Header';
import Sidebar from '../presentationals/layout/sidebar/Sidebar';
import Footer from '../presentationals/layout/Footer';

export default (MainContainer) => {
  class Dashboard extends Component {
    render(){
      const { user, match, logout, links } = this.props;
      return(
        <div >
    			<Sidebar
            user={user}
            links={links}
            currentPath={match.path}
            logout={logout}
          />
    			<Header/>
          <MainContainer {...this.props} footer={<Footer/> }/>
    		</div>
      )
    }
  }
  const mapStateToProps = ({ accounts }) => {
  	return {
      authenticating: accounts.authenticating,
  		authenticated: accounts.authenticated,
      authError: accounts.error,
      user: accounts.user
  	}
  }
  const mapDispatchToProps = (dispatch) => ({
  	logout: () => dispatch(logout())
  })
  return connect(mapStateToProps, mapDispatchToProps)(Dashboard)
}
