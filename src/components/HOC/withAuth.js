import React, { Component } from 'react';
import { connect } from 'react-redux';
import { logout, updateAccount } from '../../actions/accounts';
import { isEmpty } from '../../tools/util';
import FarmerEmptyDetails from '../presentationals/accounts/forms/FarmerEmptyDetails';

export default (authorized, links) => (Wrapper) => {
  class Auth extends Component {
    componentWillMount(){
      const { authenticated, user, logout } = this.props;
      if(!authenticated  || isEmpty(user)){
        logout();
        this.props.history.replace('/login')
      }
    }
    componentWillUpdate(nextProps){
      const { authenticated, user } = nextProps;
      if(!authenticated || isEmpty(user)){
        logout();
        this.props.history.replace('/login')
      }

    }
    render () {
      const { user, updateAccount }  = this.props;
      switch (user.type.toLowerCase()) {
        case 'farmer':
          if (isEmpty(user.details)) return (
            <FarmerEmptyDetails action={updateAccount} update target={user._id} { ...this.props }/>
          )
          return <Wrapper { ...this.props } links={links.farmer} />;
          break;
        //
        case 'company':
          if (isEmpty(user.details)) return (
            <FarmerEmptyDetails action={updateAccount} update target={user._id} { ...this.props }/>
          )
          return <Wrapper { ...this.props } links={links.company} />;
          break;
        //
        case 'admin':
          if (isEmpty(user.details)) return (
            <FarmerEmptyDetails action={updateAccount} update target={user._id} { ...this.props }/>
          )
          return <Wrapper { ...this.props } links={links.admin} />;
          break;
        //
        default:
          return <Wrapper { ...this.props } links={links} />;
      }
    }
  }
  function mapStateToProps  (store){
  	const { accounts } = store;
  	return  {
      authenticated: accounts.authenticated,
      user: accounts.user
    };
  }
  function mapDispatchToProps  (dispatch){
  	return {
  		logout: () => dispatch(logout()),
      updateAccount: (id, data) =>  dispatch(updateAccount(id, data))
  	}
  }
  return connect(mapStateToProps, mapDispatchToProps)(Auth)
}
