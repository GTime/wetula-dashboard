import React, { Component } from 'react';
import { connect } from 'react-redux';
import { isEmpty } from '../../tools/util';

export default (Wrapper, allowed = []) => {
  class Classified extends Component {
    render(){
      const { user } = this.props;
      return isAllowed(user.roles, allowed)? <Wrapper { ...this.props }/> : null;
    }
  }

  function mapStateToProps  (store){
  	const { auth } = store;
  	return {
  		user: auth.user
  	}
  }
  function mapDispatchToProps  (dispatch){
  	return {
  		actions: {}
  	}
  }

  return connect(mapStateToProps, mapDispatchToProps)(Classified);
}

const isAllowed = (roles, allowed) => {
  if (isEmpty(roles) || isEmpty(allowed)) {
    return false;
  }
  const isAllowed = roles.find(role => allowed.includes(role.name));
  return !isEmpty(isAllowed)? true : false;
}
