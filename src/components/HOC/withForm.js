import React, { Component } from 'react';
// import PropTypes  from 'prop-types';

export default (Wrapper, action) => {
  return class extends Component {
    constructor(props) {
      super(props);
      this.state = {}
  		this.handleInput = this.handleInput.bind(this);
  		this.handleSubmit = this.handleSubmit.bind(this);
    }
    //Input Field
  	handleInput(e) {
  		this.setState({ [e.target.name]:  e.target.value });
  	}
    //Handling form submit
  	handleSubmit(e ) {
  		e.preventDefault();
      //Initializing errors in state
      this.setState({ errors: {} });
  		//Send State
      action(this.state);
  	}
    render () {
      const formProps = {
        handleInput: this.handleInput,
        handleSubmit: this.handleSubmit
      }
      return <Wrapper form={formProps}  { ...this.props }/>;
    }
  }
}
