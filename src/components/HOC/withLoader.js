import React, { Component } from 'react';
import { isEmpty } from '../tools/utilities';

// Components
import LinearPreloader from '../components/extra/LinearPreloader'

export default  (propName) => (Wrapper, action) => {
	return class extends Component {
		render(){
			const { fetching, fetched, error } = this.props;
			// Checking if  fetching
			if(fetching){
				return (
					<div>
						<LinearPreloader/>
						<Wrapper {...this.props}/>
					</div>
				)
			}

      // Checking if data was fetched
			if (fetched){
				return !isEmpty(this.props[propName]) ? <Wrapper {...this.props}/> : null;
			}

			return <h2>An Error Occured</h2>
		}
	}
}
