import React, { Component } from 'react';
import { connect } from 'react-redux';
import { logout } from '../../actions/accounts';
import { isEmpty } from '../../tools/util';
import FarmerForm from '../presentationals/personalForms/Farmer';

export default (authorized) => (Wrapper) => {
  class Auth extends Component {
    componentWillMount(){
      const { user, logout } = this.props;
      if(!authorized.includes(user.type)){
        logout();
        this.props.history.replace('/login')
      }
    }
    componentWillUpdate(nextProps){
      const { user, logout } = this.props;
      if(!authorized.includes(user.type)){
        logout();
        this.props.history.replace('/login')
      }

    }
    render () {
      return <Wrapper { ...this.props } />;
    }
  }
  function mapStateToProps  (store){
  	const { accounts } = store;
  	return  {
      authenticated: accounts.authenticated,
      user: accounts.user
    };
  }
  function mapDispatchToProps  (dispatch){
  	return {
  		logout: () => dispatch(logout())
  	}
  }
  return connect(mapStateToProps, mapDispatchToProps)(Auth)
}
