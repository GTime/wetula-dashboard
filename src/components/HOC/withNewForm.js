import React, { Component } from 'react';
// import PropTypes  from 'prop-types';
import { isEmpty } from '../../tools/util';

const initValidator = props => {
  return {
     errors: {},
     isValid: true
  }
};
export default (initialState = {}, Validator = initValidator) => (Form) => {
  return class extends Component {
    constructor(props) {
      super(props);
      this.state = initialState;
  		this.handleChange = this.handleChange.bind(this);
  		this.handleSubmit = this.handleSubmit.bind(this);
      this.handleClear = this.clear.bind(this);
    }
    isValid(){
      const { errors, isValid } = Validator(this.state);
      if(!isValid){
        this.setState({ errors })
      }
      return isValid
    }
    clear() {
      this.setState({...initialState});

      // Reseting redux state
      if (this.props.reset) {
        this.props.reset();
      }
    }
  	handleChange(e) {
      const { target } = this.props;

      if (target && !isEmpty(target)) {
        this.setState({
          ...target,
          [e.target.name]:  e.target.value
        });
      }
      else {
        this.setState({
          [e.target.name]:  e.target.value
        });
      }
  	}
  	handleSubmit(e ) {
  		e.preventDefault();
      this.isValid()

      if(this.isValid()){
        delete this.state.errors;
        if (this.state.hasOwnProperty('passwordAgain')) {
          delete this.state.passwordAgain;
        }
        const data = this.state;
        this.setState({ errors: {} });
        this.props.add(this.state)
      }
  	}
    render () {
      return <Form
        state={this.state}
        handleChange={this.handleChange}
        handleSubmit={this.handleSubmit}
        clear={this.handleClear}
        { ...this.props }
      />;
    }
  }
}
