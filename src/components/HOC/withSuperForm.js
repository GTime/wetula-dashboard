import React, { Component } from 'react';
import { isEmpty } from '../../tools/util';

const initValidator = props => {
  return {
     errors: {},
     isValid: true
  }
};
export default (initialState = {}, Validator = initValidator) => (Form) => {
  return class extends Component {
    constructor(props) {
      super(props);
      this.state = { ...initialState, enableSubmit: false };
  		this.handleChange = this.handleChange.bind(this);
  		this.handleSubmit = this.handleSubmit.bind(this);
      this.handleClear = this.handleClear.bind(this);
    }
    isValid() {
      const { errors, isValid } = Validator(this.state);
      if(!isValid) {
        this.setState({ errors });
      }
      return isValid;
    }
    handleClear() {
      this.setState({
        ...initialState,
        enableSubmit: false
      });

      // Reseting redux state
      if (this.props.reset) this.props.reset();
    }
    clean(items = []) {
        items.forEach(item => {
          if (this.state.hasOwnProperty(item)){
            delete this.state[item];
          }
        });
    }
  	handleChange(e)  {
      if (!isEmpty(e.target.value)) this.setState({ enableSubmit: true });
  		this.setState({ [e.target.name]:  e.target.value });
      this.clean(['enableUpdate']);
  	}
    handleSubmit(e) {
      const { target, update, action } = this.props;
  		e.preventDefault();
      this.isValid();


      if(this.isValid() && !isEmpty(this.state)){
        this.clean(['enableSubmit','errors', 'passwordVerify']);
        const data = this.state;
        this.setState({
          errors: {},
          enableUpdate: true
        });

        if (update) {
          action(target, this.state);
        }
        else {
          action(this.state);
        }
      }
  	}
    render () {
      return (
        <Form
          state={ this.state }
          handleChange={ this.handleChange }
          handleSubmit={ this.handleSubmit }
          clear={ this.handleClear }
          { ...this.props }/>
      );
    }
  }
}
