import React from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import withAuth from './HOC/withAuth';
import Login from './Login';
import Farmers from './Farmers';
import ScaleMeasurements from './ScaleMeasurements';
import AccountDetail from './AccountDetail';
// import FarmerDetail from './FarmerDetail';
import * as links from '../links';

const level2 = withAuth(['company', 'admin'], links);

export default () => {
	return (
		<Router>
			<Switch>
				<Route path="/" exact component={Login}/>
				<Route path="/login" exact component={Login}/>
				<Route path="/farmers" exact component={level2(Farmers)}/>
				<Route path="/accounts/:id" exact component={level2(AccountDetail)}/>
				{/* <Route path="/farmers/:id" exact component={level2(FarmerDetail)}/> */}
				<Route path="/scaleMeasurements" exact component={level2(ScaleMeasurements)}/>
				{/* <Route path="/farmers" exact component={Manager(Farmers)}/> */}
				<Route component={NotFound}/>
			</Switch>
		</Router>
	);
}

const NotFound = () => (
	<h1>Not Found</h1>
);

const Land = () => (
	<h1>Welcome</h1>
);
