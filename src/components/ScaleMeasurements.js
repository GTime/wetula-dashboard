import React, { Component } from 'react';
import { connect } from 'react-redux';
import { fetchScaleMeasurements } from '../actions/scaleMeasurements';
import { isEmpty } from '../tools/util';

import withDashboard from './HOC/withDashboard';
import Header from './presentationals/headers/Header';
import Pagination from './presentationals/Pagination';
import ScaleMeasurement from './presentationals/scaleMeasurements/ScaleMeasurement';
import Preloader from './presentationals/extra/LinearPreloader';
import NoData from './presentationals/extra/NoData';


class UI extends Component {
	componentDidMount() {
		this.props.fetchScaleMeasurements();
	}
	render() {
		const { scaleMeasurements, fetching, fetched, error } = this.props;
		const formated = scaleMeasurements.map((scaleMeasurement, key) => <ScaleMeasurement
			index={key + 1}
			{ ...scaleMeasurement }/>
		);
		const Fields = [
			'#', 'Farmer', 'Weight', 'Date'
		].map((title, key) => <th key={key}>{ title }</th>);

		return <main className="main-container">
			<Header title='Scale Measurements' subtitle='Explore all Scale Measurements'/>
			<section className="main-content">
			<Preloader active={fetching}/>
			<div className="card">
					{
						isEmpty(scaleMeasurements)?
						<div className="card-body">
							<NoData message='No weight recorded yet'/>
						</div>:
						<div className="card-body">
							<table className="table table-separated">
								<thead>
									<tr> { Fields } </tr>
								</thead>
								<tbody>
									{ formated }
								</tbody>
							</table>
							{/* <TablePagination/> */}
						</div>
					}
			</div>
			</section>
		</main>
	}
}

const mapStateToProps = ({ scaleMeasurements }) => {
	return {
		scaleMeasurements: scaleMeasurements.scaleMeasurements,
		fetching: scaleMeasurements.fetching,
		fetched: scaleMeasurements.fetched,
		error: scaleMeasurements.error
	}
}
const mapDispatchToProp = (dispatch) => ({
	fetchScaleMeasurements: () => dispatch(fetchScaleMeasurements())
})
const Conneted = connect(mapStateToProps, mapDispatchToProp)(UI);
export default withDashboard(Conneted);


const TablePagination = () => (
	<nav>
		<ul className="pagination justify-content-center">
			<li className="page-item disabled">
				<a className="page-link" href="#">
					<span className="ti-arrow-left"></span>
				</a>
			</li>
			<li className="page-item active">
				<a className="page-link" href="#">1</a>
			</li>
			<li className="page-item"><a className="page-link" href="#">2</a></li>
			<li className="page-item"><a className="page-link" href="#">3</a></li>
			<li className="page-item"><a className="page-link" href="#">4</a></li>
			<li className="page-item"><a className="page-link" href="#">5</a></li>
			<li className="page-item">
				<a className="page-link" href="#">
					<span className="ti-arrow-right"></span>
				</a>
			</li>
		</ul>
	</nav>
)
