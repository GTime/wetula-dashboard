import React from 'react';

export default ({ index, account, weight, created }) => {
	const date = new Date(created).toDateString();
	return (
		<tr>
			<th scope="row">{index}</th>
			<td>{weight}kg</td>
			<td>{date}</td>
		</tr>
	);
}
