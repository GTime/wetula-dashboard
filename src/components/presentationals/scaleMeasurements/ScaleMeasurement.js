import React from 'react';

export default ({ index, account, weight, created }) => {
	const { _id, userName, details } = account;
	const date = new Date(created).toDateString();
	return (
		<tr>
			<th scope="row">{index}</th>
			<td>
				<div className="media">
					<img className="avatar" src="/assets/img/staffs/default.jpg" alt="..."/>
					<div className="media-body">

							<p className="lh-1">
								<a href={'/accounts/' + _id}>
									{ userName }
								</a>
							</p>
							{
								details?
								<small>{ details.firstName } { details.lastName }</small>:
								<p>Account details has not been updated</p>
							}
					</div>
				</div>
			</td>
			<td>{weight}kg</td>
			<td>{date}</td>
		</tr>
	);
}
