import React from 'react';
import ScaleMeasurement from './ScaleMeasurement2';
import NoData from '../extra/NoData';

export default ({ scaleMeasurements }) => {
	const formated = scaleMeasurements.map((scaleMeasurement, key) =><ScaleMeasurement
		key={key}
		index={key + 1}
		{ ...scaleMeasurement }/>
	);
	const Fields = [
		'#', 'Weight', 'Date'
	].map((title, key) => <th key={key}>{ title }</th>);

	return(
		<div className="card">
			<div className='card-body bb-1 border-fade'>
				<h4 className=" opacity-90">
					<strong>Scale Measurements</strong>
				</h4>

				{
					scaleMeasurements.length == 0 ?
					<NoData message='No weight recorded yet'/>:
					<div>
						<table className="table table-separated">
							<thead>
								<tr> { Fields } </tr>
							</thead>
							<tbody>
								{ formated }
							</tbody>
						</table>
						<TablePagination/>
					</div>
				}

			</div>
		</div>
	)
}

const TablePagination = () => (
	<nav>
		<ul className="pagination justify-content-center">
			<li className="page-item disabled">
				<a className="page-link" href="#">
					<span className="ti-arrow-left"></span>
				</a>
			</li>
			<li className="page-item active">
				<a className="page-link" href="#">1</a>
			</li>
			<li className="page-item"><a className="page-link" href="#">2</a></li>
			<li className="page-item"><a className="page-link" href="#">3</a></li>
			<li className="page-item"><a className="page-link" href="#">4</a></li>
			<li className="page-item"><a className="page-link" href="#">5</a></li>
			<li className="page-item">
				<a className="page-link" href="#">
					<span className="ti-arrow-right"></span>
				</a>
			</li>
		</ul>
	</nav>
)
