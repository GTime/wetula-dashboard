import React  from 'react';

export default (props) => (
	<main className="main-container">
		<div className="main-content" style={style.center}>
			<h1>Successfully Deleted</h1>
			<a href={props.link} className='btn btn-primary'>Go back to {props.title}</a>
		</div>
	</main>
)

const style = {
	center: {
		display: 'flex',
		flexDirection: 'column',
		justifyContent: 'center',
		alignItem: 'center',
		height: "84vh",
		textAlign: 'center'
	}
}
