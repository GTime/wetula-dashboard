import React from 'react'

export default ({ message }) => (
	<div>
		<br/>
		<h5 className=" opacity-70 text-center ">{message}</h5>
		<br/>
	</div>
)
