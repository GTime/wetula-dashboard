import React, { Component }  from 'react';

// Components
import SmartButton from './SmartButton'
import SwitchView from './SwitchView'

export default class extends Component {
	constructor(props){
		super(props);
		this.state = {
			activeLink: 'main'
		}
		this.changeView = this.changeView.bind(this)
	}
	componentDidUpdate(){
		if (this.props.loaded) {
			this.setState({
				activeLink: 'success'
			})
			setInterval(() => {
				this.setState({
					activeLink: 'main'
				})
			}, 300);
		}
	}
	changeView(e){
		const { link } = e.target.dataset;
		if (link) {
			this.setState({ activeLink: link})
		}
	}
	render(){

		const viewProps = {
			...this.props,
			changeView:this.changeView
		}
		const switchProps = {
			activeLink: this.state.activeLink,
			views: [
				{link: 'main', component: Main, props: viewProps},
				{link: 'confirm', component: Confirm, props: viewProps},
				{link: 'success', component: Success}
			]
		}
		return(
			<div className='card'>
				<SwitchView {...switchProps} />
			</div>
		)
	}
}

function Main(props){
	return(
		<div className='card-body bb-1 border-fade'>
			<small className=' opacity-60'>{props.title}</small>
			<button
				data-link='confirm'
				onClick={props.changeView}
				type='button'
				className='btn btn-block btn-danger'>{props.title}
			</button>
		</div>
	)
}
function Confirm(props){
	const { action, loading, error, target='', title='' } = props;
	const style = {
		display: 'flex',
		justifyContent: 'space-around'
	}
	return(
		<div className='card-body bb-1 border-fade'>
			<h6>Are your sure you want to {title.toLowerCase()}?</h6>
			<div style={style}>
				<button
					data-link='main'
					onClick={props.changeView}
					type="button"
					className="btn btn-bold btn-secondary"
					data-selected={target}>No
				</button>
				{
					 (error !== null) ?
					 <SmartButton
						 dataSelected={target}
						 onClick={action}
						 type='button'
						 title='Retry'
						 isLoaoding={loading}
						 className='btn-danger'
						 disabled={false} /> :
					 <SmartButton
						 dataSelected={target}
						 onClick={action}
						 type='button'
						 title='Yes'
						 isLoaoding={loading}
						 className='btn-danger'
						 disabled={false} />
				 }
			</div>
		</div>
	)
}
function Success(props){
	return(
		<div className='card-body bb-1 border-fade'>
			<br/><br/>
			<h3>Successful</h3>
			<br/><br/>
		</div>
	)
}
