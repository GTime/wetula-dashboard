import React from 'react'

export default (props) => {
	const { title, value, link, statistics, progress, width, flag } = props;
	const flagColors = (flag === 'good') ? {bg: ' bg-succes ', text: ' text-succes '} : {bg: ' bg-danger ', text: ' text-danger '}
	return(
		<div className={"col-lg-" + width}>
      <div className="card card-body">
        <h6>
          <span className="text-uppercase">{title}</span>
          {
						link ?
						<span className="float-right">
							<a className="btn btn-xs btn-primary" href={link}>
								View
							</a>
						</span> : null 
					}
        </h6>
        <br/>
        <p className="fs-28 fw-100">{value}</p>
        <div className="progress">
          <div
						className={'progress-bar ' + flagColors.bg}
						role="progressbar" style={{width: '35%', height: '4px'}}
						aria-valuenow={progress}
						aria-valuemin="0"
						aria-valuemax="100"></div>
        </div>
        {
					statistics ?
					<div className="text-gray fs-12">
						<i className={'ti-stats-down mr-1 ' + flagColors.text}></i> {statistics}
					</div> :
					null
				}
      </div>
    </div>
	)
}
