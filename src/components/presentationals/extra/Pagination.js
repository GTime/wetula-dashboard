import React from 'react';

// Components

export default  (props) => {
  const { handleMore } = props;
	return(
		<nav className="mt-3">
			<ul className="pagination justify-content-center">

				<li className="page-item">
				 <button className="page-link" onClick={handleMore}> More</button>
			 </li>

			 {/* <li className="page-item disabled">
				<a className="page-link" href="#">Everything</a>
			</li> */}

			</ul>
		</nav>
	)
}
