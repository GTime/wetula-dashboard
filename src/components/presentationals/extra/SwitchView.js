import React from 'react';

// Components
export default (props) => {
	const { activeLink, views } = props
	
	let Wrapper = null
	const view = (views && activeLink)? views.find((view) => view.link === activeLink) : null
	if(view !== null || view !== undefined){
		const properties = view.props ? view.props : {}
		Wrapper = view.component
		return <Wrapper {...properties}/>
	}
	else {
		return null
	}
}
