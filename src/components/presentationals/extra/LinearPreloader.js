import React from 'react';

//  LinearPreloader
export default ({ active }) => {
	if (active === true) {
		return<div className="spinner-linear">
			<div className="line"></div>
		</div>
	}
	return null
}
