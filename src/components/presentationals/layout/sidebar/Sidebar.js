import React  from 'react';

// Components
import SidebarMenu from './SidebarMenu';
import SidebarProfile from './SidebarProfile';

/*-- Default --*/
export default (props) => {
	const { user, links, logout, currentPath } = props;

	return (
		<aside className="sidebar sidebar-icons-right sidebar-icons-boxed sidebar-expand-lg">
      <header className="sidebar-header">
        <a className="logo-icon" href="/">W</a>
        <span className="logo">
          <a href="/">WETULA</a>
        </span>
        <span className="sidebar-toggle-fold"></span>
      </header>

      <nav className="sidebar-navigation"  style={{ overflowY: 'auto' }}>
        <SidebarProfile user={user} logout={logout}/>
        <SidebarMenu links={links} currentPath={currentPath}/>
      </nav>
    </aside>
	);
}
