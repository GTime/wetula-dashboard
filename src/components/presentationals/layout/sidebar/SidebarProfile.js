import React  from 'react';

// Components
import SidebarMenu from './SidebarMenu';

/*-- Default --*/
export default (props) => {
	const { user, logout } = props;

	return (
		<div className="sidebar-profile">
			<div className="dropdown">
				<span className="dropdown-toggle no-caret" data-toggle="dropdown">
					<img className="avatar" src={'/assets/img/staffs/default.jpg'} alt="."/>
				</span>
				<div className="dropdown-menu open-top-center">
					<a className="dropdown-item" href={'/farmers/' + user.id}>
						<i className="ti-user"></i>
						Profile
					</a>
					<div className="dropdown-divider"></div>
					<a className="dropdown-item" href="#" onClick={logout}><i className="ti-power-off"></i> Logout</a>
				</div>
			</div>
			<div className="profile-info">
				<h4>{user.userName}</h4>
				<p style={style.capitalize}>{user.type}</p>
			</div>
		</div>
	);
}

const style = {
	capitalize: {
		textTransform: 'capitalize'
	}
}
