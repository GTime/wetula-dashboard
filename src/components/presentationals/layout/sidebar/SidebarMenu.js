import React  from 'react';


/*-- Default --*/
export default (props) => {
	const { links, currentPath } = props; console.log(currentPath);
	const menus =  links.map((link, key) => {
		if (link.relative === true) {

			return (currentPath.includes(link.path) ) ?
			<MenuItem key={key} title={link.title} path={link.path} icon={link.icon} active/>:
			<MenuItem key={key} title={link.title} path={link.path} icon={link.icon}/>
		}
		else {
			return (currentPath == link.path ) ?
			<MenuItem key={key} title={link.title} path={link.path} icon={link.icon} active/>:
			<MenuItem key={key} title={link.title} path={link.path} icon={link.icon}/>
		}
	})

	return (
		<ul className="menu menu-lg menu-bordery">
			{ menus }
		</ul>
	);
}

// Menu Item
const MenuItem = (props) => {
	const { title, path, icon, updateHint, active } = props;
	const activeClass = active ? "active" : "";
	const update = updateHint > 0 ? <small>{updateHint} new update</small> : null;

	return(
		<li className={"menu-item " + activeClass}>
			<a className="menu-link" href={path}>
				<span className={"icon ti-" + icon.trim()}></span>
				<span className="title">
					<span>{title}</span>
					{update}
				</span>
			</a>
		</li>
	)
}
