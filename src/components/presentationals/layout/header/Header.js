import React, { Component } from 'react';
import { limiter } from '../../../../tools/util';

export default (props) => {
	const { currentPath, notifications, actions } = props;
	// const allNotifications = limiter(notifications, 8).map((item, key) => {
	// 	return <Notification key={key} {...item} seen={actions.seen}/>
	// });
	// const hasNew = (allNotifications.length > 0) ? 'has-new' : null;
	const hasNew = 'has-new';
	return (

		<header className="topbar">
			<div className="topbar-left">
				<span className="topbar-btn sidebar-toggler"><i>&#9776;</i></span>
			</div>

			<div className="topbar-right">
				<div className="topbar-divider"></div>

				<ul className="topbar-btns">
					{/* --Notifications --*/}
					<li className="dropdown d-none d-md-block">
						<span className={"topbar-btn " + hasNew} data-toggle="dropdown">
							<i className="ti-bell"></i>
						</span>
						<div className="dropdown-menu dropdown-menu-right">

							{/* <div className="media-list media-list-hover media-list-divided media-list-xs">
								{ allNotifications }
							</div> */}

							<div className="dropdown-footer">
								<div className="left">
									<a href="#">Read all notifications</a>
								</div>

								<div className="right">
									<a href="#" data-provide="tooltip" title="Mark all as read"><i className="fa fa-circle-o"></i></a>
									<a href="#" data-provide="tooltip" title="Update"><i className="fa fa-repeat"></i></a>
									<a href="#" data-provide="tooltip" title="Settings"><i className="fa fa-gear"></i></a>
								</div>
							</div>

						</div>
					</li>
					{/* -- END Notifications -- */}
				</ul>

			</div>
		</header>
	);
}

// Notifications
function Notification (props) {
	let body = '';

	switch (props.type) {
		case 'Finished':
			body = (
								<div className="media-body">
									<p>
										{props.product.name} is to be or already out of stock
									</p>
									<span>Quanitity in stock: {props.product.quantityInStock}</span><br/>
									<time dateTime={props.created_at}>{new Date(props.created_at).toLocaleString()}</time>
								</div>
							);
			break;
		case 'Expired':
			body = (
							<div className="media-body">
								<p>
									{props.product.name} is about to be or already expired
								</p>
								<time>Expiry Date: {props.product.expDate}</time><br/>
								<time dateTime={props.created_at}>{new Date(props.created_at).toLocaleString()}</time>
							</div>
						);
			break;
		default:

	}

	return(
		<a className="media media-new" href="#" onClick={() => props.seen(props.id)}>
			<span className="avatar bg-danger">
				<i className="ti-heart"></i>
			</span>
			{ body }
		</a>
	)
}
