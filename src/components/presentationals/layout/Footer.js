import React from 'react';


export default () => {
	return (
		<footer className="site-footer">
			<div className="row">
				<div className="col-md-6">
					<p className="text-center text-md-left">Copyright © 2018 <a href="#"></a>. All rights reserved.</p>
				</div>

				<div className="col-md-6">
					<ul className="nav nav-primary nav-dotted nav-dot-separated justify-content-center justify-content-md-end">
						<li className="nav-item">
							<a className="nav-link" href="../help/faq.html">FAQ</a>
						</li>
					</ul>
				</div>
			</div>
		</footer>
	);
}
