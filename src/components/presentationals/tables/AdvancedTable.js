import React from 'react';
import withTable from './HOC/withTable';

const action = 'Action Here';
const UI = (props) => {
  const { content, tableEvent } = props;
  const { data, headers } = content;
  return(
    <div className="row mt">
        <div className="col-md-12">
            <div className="content-panel">
                <table className="table table-striped table-advance table-hover">
                    <TableHead headers={headers}/>
                    <TableBody data={data} tableEvent={tableEvent}/>
                </table>
            </div>
        </div>
    </div>
  )
}
export default withTable(UI, action);

// Table Head
const TableHead = (props) => {
  const { headers } = props;
  const allHeader = headers.map((header, key) => {
    if(header !== "Descrition" || header !== "note"){
      return <th key={key}>{header}</th>
    }
    return <th key={key} className="hidden-phone">{header}</th>
  });
  return(
    <thead>
      <tr>
          {allHeader}
      </tr>
    </thead>
  );
}

// Table Body
const TableBody = (props) => {
  const { data, tableEvent } = props;
  const rows =  data.map((item, key) => <Row key={key} item={item} tableEvent={tableEvent}/>);
  return(
    <tbody>
      {rows}
    </tbody>
  )
}

// Table Row
const Row = (props) => {
  const { item, tableEvent } = props;
    const { handleEdit, handleDelete } = tableEvent;
  const { id, firstName, lastName, email } = item;
  const url = "";
  return(
    <tr>
        <td>{id}</td>
        <td><a href="basic_table.html#">{firstName} {lastName}</a></td>
        <td className="hidden-phone">{email}</td>
        <td>
            <a title="View" onClick={handleEdit} href={url} className="btn btn-success btn-xs"><i href={url} className="fa fa-eye"></i></a>
            <button title="Edit" onClick={handleEdit} data-target={id} className="btn btn-primary btn-xs"><i data-target={id} className="fa fa-pencil"></i></button>
            <button title="Delete" onClick={handleDelete} data-target={id} className="btn btn-danger btn-xs"><i data-target={id} className="fa fa-trash-o "></i></button>
        </td>
    </tr>
  );
}
