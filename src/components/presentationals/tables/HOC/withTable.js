import React, { Component } from 'react';

export default (Wrapper, action) => {
  return class extends Component {
    constructor(props) {
      super(props);
      this.state = {},
  		this.handleEdit = this.handleEdit.bind(this);
  		this.handleDelete = this.handleDelete.bind(this);
    }
    // Handle Edit
  	handleEdit(e) {
      const { target } = e.target.dataset;
  		this.setState({ target });
      window.alert("You want to Edit: " + target);
  	}
    //Handling Delete
  	handleDelete(e ) {
      const { target } = e.target.dataset;
      this.setState({ target });
      window.alert("You want to Delete: " + target);
  	}
    render () {
      const tableProps = {
        handleEdit: this.handleEdit,
        handleDelete: this.handleDelete
      }
      return <Wrapper tableEvent={tableProps}  { ...this.props }/>;
    }
  }
}
