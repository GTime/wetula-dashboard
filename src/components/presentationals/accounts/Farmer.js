import React from 'react';

export default ({ _id, userName, details }) => {

	return (
		<div className="media">
			<a className="avatar avatar-lg status-success" href="#">
				<img src="/assets/img/staffs/default.jpg" alt="..."/>
			</a>

			<div className="media-body">
				<p>
					<a className="hover-primary" href={'/accounts/' + _id}>
						<strong>{ userName }</strong>
					</a>
					{ details && <small className="sidetitle">{ details.email }</small>}
				</p>
				{
					details?
					<p>{ details.firstName } { details.lastName }</p>:
					<p>Account details has not been updated</p>
				}

				{/* <nav className="nav mt-2">
					<a className="nav-link" href="#"><i className="fa fa-facebook"></i></a>
					<a className="nav-link" href="#"><i className="fa fa-twitter"></i></a>
					<a className="nav-link" href="#"><i className="fa fa-github"></i></a>
					<a className="nav-link" href="#"><i className="fa fa-linkedin"></i></a>
				</nav> */}
			</div>

			<div className="media-right gap-items">
				<div className="btn-group">
					<a className="media-action lead" href="#" data-toggle="dropdown"><i className="ti-more-alt rotate-90"></i></a>
					<div className="dropdown-menu dropdown-menu-right">
						<a className="dropdown-item" href="#"><i className="fa fa-fw fa-user"></i> Profile</a>
						{/* <a className="dropdown-item" href="#"><i className="fa fa-fw fa-comments"></i> Messages</a>
						<a className="dropdown-item" href="#"><i className="fa fa-fw fa-phone"></i> Call</a> */}
						<div className="dropdown-divider"></div>
						<a className="dropdown-item" href="#"><i className="fa fa-fw fa-remove"></i> Remove</a>
					</div>
				</div>
			</div>
		</div>
	);
}
