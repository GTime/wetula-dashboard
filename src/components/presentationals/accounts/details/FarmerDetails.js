import React from 'react';
import DetailScaleMeasurements from '../../scaleMeasurements/DetailScaleMeasurements';

export default (props) => {
	const { measurements } = props;
	return(
		<div className='row'>
			<div className='col-md-5'>
				<AccountInfo {...props}/>
				<PersonalInfo {...props}/>
				<ContactInfo {...props}/>
				<Extra/>
			</div>
			<div className='col-md-7'>
				{ measurements ?
					<DetailScaleMeasurements scaleMeasurements={ measurements }/>:
					null
				}
			</div>
		</div>
	)
}

const AccountInfo = ({ userName, type }) => (
	<div className="card">
		<div className='card-body bb-1 border-fade'>
			<h4 className=" opacity-90">
				<strong>Account Information</strong>
			</h4><br/>
			<p>
				<span className=" opacity-60">UserName: </span>  {userName}
			</p>
			<p style={{textTransform: 'capitalize'}}>
				<span className=" opacity-60">Account type: </span> { type }
			</p>
		</div>
	</div>
)
const PersonalInfo = ({ details }) => (
	<div className="card">
		<div className='card-body bb-1 border-fade'>
			<h4 className=" opacity-90">
				<strong>Personal Information</strong>
			</h4><br/>
			<p style={{textTransform: 'capitalize'}}>
				<span className=" opacity-60">Gender: </span>
				{ details.gender }
			</p>
			<p>
				<span className=" opacity-60">Date of birth: </span>
				{ new Date(details.dob).toDateString() }
			</p>
		</div>
	</div>
)
const ContactInfo = ({ details }) => (
	<div className="card">
		<div className='card-body bb-1 border-fade'>
			<h4 className=" opacity-90">
				<strong>Contact Information</strong>
			</h4><br/>
			<p>
				<span className=" opacity-60">Email: </span> { details.email }
			</p>
			<p>
				<span className=" opacity-60">Phone Number: </span> { details.phone }
			</p>
			<p>
				<span className=" opacity-60">Address: </span> { details.address }
			</p>
			<p>
				<span className=" opacity-60">City: </span> { details.city }
			</p>

		</div>
	</div>
)
const Extra = () => (
	<div className="card">
		<div className='card-body bb-1 border-fade'>
			<h4 className=" opacity-90">
				<strong>More Information</strong>
			</h4><br/>

			<p>
				Lorem ipsum dolor sit amet, consectetur adipisicing elit,
				sed do eiusmod tempor incididunt ut labore et.
				Lorem ipsum dolor sit amet, consectetur adipisicing elit,
				sed do eiusmod tempor incididunt ut labore et.
			</p>

			<p>
				Lorem ipsum dolor sit amet, consectetur adipisicing elit,
				sed do eiusmod tempor incididunt ut labore et.
				Lorem ipsum dolor sit amet, consectetur adipisicing elit,
				sed do eiusmod tempor incididunt ut labore et.
			</p>

		</div>
	</div>
)
