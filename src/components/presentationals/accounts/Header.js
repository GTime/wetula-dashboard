import React from 'react'
import { isEmpty} from '../../../tools/util'
import LinearPreloader from '../extra/LinearPreloader'

export default (props) => {
  const { nav, detail } = props;

	return(
		<header className="header bg-img" style={{backgroundImage: "url('/assets/img/gallery/8.jpg')"}}>
			<div className="header-info h-100px mb-0">
        { !isEmpty(detail)? <ProfileMedia {...detail}/> : <LinearPreloader/>}
			</div>

			<div className="header-action bg-white">
				<Nav {...nav}/>
			</div>
		</header>
	)
}

function Nav(props) {
  const { links, activeLink, changeView } = props

  if(links){
    const allNav = links.map((link, key) => {
      return (link.link === activeLink) ?
             <Navlink key={key} {...link} changeView={changeView} active /> :
             <Navlink key={key} {...link} changeView={changeView}/>
    })
    return <nav className="nav">{allNav}</nav>
  }
  else {
    return null
  }
}
function Navlink(props){
  const { title, link, active, changeView } = props;
  const isActive = active ? ' active ' : null
  return (
    <a
      data-link={link}
      onClick={changeView}
      role='button'
      className={'nav-link ' + isActive}>
      {title}
    </a>
  )
}
const ProfileMedia = ({ userName, type, details }) => {
  let name = "Account details has not been updated";

  if (!isEmpty(details)) {
    switch (type) {
      case 'farmer':
        name = details.firstName + ' ' + details.lastName;
        break;
      //
      case 'company':
        name = details.name
        break;
      //
      default:

    }
  }

  return (
    <div className="media align-items-end">
      <img
        className="avatar avatar-xl avatar-bordered"
        src={"/assets/img/staffs/default.jpg"}
        alt="..."
      />
      <div className="media-body">
        <p className="text-white opacity-90">
          <strong>{ userName }</strong>
        </p>
        <small className="text-white opacity-60">{ name }</small>
      </div>
    </div>
  )
}
