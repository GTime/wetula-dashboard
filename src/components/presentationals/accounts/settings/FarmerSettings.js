import React from 'react'
import UpdateForm from '../forms/UpdateForm';
import UpdateAccountForm from '../forms/UpdateAccountForm';
// import ChangePasswordForm from './forms/ChangePasswordForm'
// import SmartButton from '../buttons/SmartButton'
// import Confirmation from '../extra/Confirmation'
import withClassified from '../../../HOC/withClassified';

export default ({
	detail,
	update,
	updateAccount,
	reset
}) => {
	// const disableProps = {
	// 	title: 'Disable Account',
	// 	target: props.profile.id,
	// 	loading: props.disable.disabling,
	// 	loaded: props.disable.disabled,
	// 	error: props.disable.error,
	// 	action: props.actions.disable,
	// }
	// const deleteProps = {
	// 	title: 'Delete Account',
	// 	target: props.profile.id,
	// 	loading: props.delete.deleting,
	// 	loaded: props.delete.deleted,
	// 	error: props.delete.error,
	// 	action: props.actions.delete,
	// }
	// const Disable = withClassified(Confirmation, ['Administrator', 'Manager']);
	// const Delete = withClassified(Confirmation, ['Administrator', 'Manager']);
	return(
		<div className='row'>
			<div className='col-md-8'>
				<UpdateForm
					detail={detail}
					{...update}
					updateAction={updateAccount}
					reset={reset}/>
			</div>
			<div className='col-md-4'>
				<UpdateAccountForm
					detail={detail}
					{...update}
					updateAction={updateAccount}
					reset={reset}/>
				{/* <ChangePasswordForm
					profile={props.profile}
					update={props.update}
					actions={{
						update: props.actions.changePassword,
						reset: props.actions.reset
					}}/> */}
				{
					// (details._id != user._id)?
					// <div>
					// 	<div className="divider fs-16">More Settings</div>
					// 	<Disable {...disableProps}/>
					// 	<Delete {...deleteProps}/>
					// </div>:
					// null
				}
			</div>
		</div>
	)
}
