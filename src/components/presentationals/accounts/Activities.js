import React from 'react'

import Activity from '../extra/ActivityCard';

export default (props) => {
	

	return(
		<div className="row">
			<Activity
				width={'4'}
				title={'Total Revenue'}
				value={'$21,642'}
				// link={'#'}
				progress={'35'}
				flag={'good'}
				statistics={'%18 increase from last month'}
			/>
			<Activity
				width={'4'}
				title={'Total Hours Worked'}
				value={'60hr'}
				// link={'#'}
				progress={'20'}
				flag={'bad'}
				statistics={'%20 increase from last month'}
			/>
			<Activity
				width={'4'}
				title={'Total Revenue'}
				value={'$21,642'}
				// link={'#'}
				progress={'35'}
				flag={'good'}
				statistics={'%18 increase from last month'}
			/>
		</div>
	)
}
