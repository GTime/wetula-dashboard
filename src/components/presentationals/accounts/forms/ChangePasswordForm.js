import React from 'react';
import withUpdateForm from '../../../HOC/withUpdateForm'
import { Input, Select } from '../../formItems/'
import SmartButton from '../../extra/SmartButton'


const UI = (props) => {
  const { form, profile, update } = props;
  const { updating, updated, error } = update;

  return(
    <form data-selected={profile.id} onSubmit={form.handleSubmit} className='card' >
      <div className='card-body bb-1 border-fade'>
        <h4 className=' opacity-90'>
          <strong>Change Password</strong>
        </h4>

        <div className='form-type-material'><br/>
          <div className='row'>
            <div className='col-md-12'>
              <Input
                className='do-float'
                name='currentPassword'
                type='password'
                required={true}
                onChange={form.handleChange}
                label='Current Password'
              />
              <Input
                className='do-float'
                name='newPassword'
                type='password'
                required={true}
                onChange={form.handleChange}
                label='New Password'
              />
              <p className="text-danger">{ error ? error.message : null }</p>
              {
                error ?
                <SmartButton type='submit' title='Retry' isLoaoding={updating} className=' btn-block btn-primary' /> :
                <SmartButton type='submit' title='Change' isLoaoding={updating}  className=' btn-block btn-primary' disabled={!form.enableUpdate}/>
              }

            </div>

          </div>
        </div>
      </div>
    </form>
  )
}
export default withUpdateForm(UI)
