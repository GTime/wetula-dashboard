import React from 'react';
import withUpdateForm from '../../../HOC/withUpdateForm'
import { Input, Select } from '../../formItems/'
import SmartButton from '../../buttons/SmartButton';

// Location
const cities = [ 'Accra',  'Kumasi', 'Tema' ];
const genders = [ 'Male',  'Female' ];

const UI = (props) => {
  const { form, detail, updating, updated, error } = props;
  const { details } = detail;
  const formatedCities = cities.map((city) => Object({title: city, value: city} ) );
  const formatedGender = genders.map((gender) => Object({title: gender, value: gender} ));

  return(
    <form data-selected={detail._id} onSubmit={form.handleSubmit} className='card' >
      <div className='card-body bb-1 border-fade'>
        <h4 className=' opacity-90'><strong>Update Info</strong></h4>
        <small className=' opacity-60'>Update detail information and save</small>

        <div className='form-type-material'><br/>
          <div className='row'>
            <div className='col-md-6'>
              <h6 className=' opacity-60'>Personal Information</h6><br/>
              <Input className='do-float' name='details.firstName' type='text' onChange={form.handleChange} defaultValue={details.firstName} label='First name'/>
              <Input className='do-float' name='details.lastName' type='text' onChange={form.handleChange} defaultValue={details.lastName} label='Last name'/>
              <Input className='do-float' name='details.nickName' type='text' onChange={form.handleChange} defaultValue={details.nickName} label='Nick name'/>
              <Input className='do-float' name='details.dob' type='date' onChange={form.handleChange} defaultValue={new Date(details.dob).toDateString()} label='Date of birth'/>
              <Select className='do-float' name='details.gender' onChange={form.handleChange} defaultValue={details.gender} label='Gender' options={formatedGender}/>
            </div>

            <div className='col-md-6'>
              <h6 className=' opacity-60'>Contact Information</h6><br/>
              <Input className='do-float' name='details.email' type='email' onChange={form.handleChange} defaultValue={details.email} label='Email Address'/>
              <Input className='do-float' name='details.phone' type='tel' onChange={form.handleChange} defaultValue={details.phone} label='Phone Number'/>
              <Input className='do-float' name='details.address' type='text'  onChange={form.handleChange} defaultValue={details.address} label='Address'/>
              <Select className='do-float' name='details.city' label='City' onChange={form.handleChange} defaultValue={details.city} options={formatedCities}/>
            </div>
            <div className='col-md-12'>
              {
                error ?
                <SmartButton type='submit' title='Retry' isLoaoding={updating} className=' btn-block btn-primary' /> :
                <SmartButton type='submit' title='Update' isLoaoding={updating}  className=' btn-block btn-primary' disabled={!form.enableUpdate}/>
              }

            </div>

          </div>
        </div>
      </div>
    </form>
  )
}
export default withUpdateForm(UI)
