import React from 'react';
import withNewForm from '../../../HOC/withNewForm';
import SmartButton from '../../buttons/SmartButton';
import { Input, Select } from '../../formItems/';
import Validator from '../../../../formValidations/NewAccount';

const initialState = {
  userName: '',
  password: '',
  verifyPassword: '',
  type: '',
  errors: {}
};
const accountTypes = [ 'Farmer',  'Company' ];
const UI = (props) => {
  return(
    <div className="modal modal-right fade" id="modal-new-farmer" tabIndex="-1">
      <div className="modal-dialog modal-sm">
        {
          (props.added === true) ?
          <Success clearform={props.clear}/> :
          <Form
            state={props.state}
            handleChange={props.handleChange}
            handleSubmit={props.handleSubmit}
            accountTypes={accountTypes}
            { ...props }
          />
        }
      </div>
    </div>
  )
}
export default  withNewForm(initialState, Validator)(UI);

const Form = (props) => {
  const { accountTypes, addError, adding, handleSubmit, handleChange, state } = props;
  const mappedAccountTypes = accountTypes.map((type) => Object({
    title: type,
    value: type
  }));

  return(
    <form onSubmit={handleSubmit} className="modal-content" >
      <div className="modal-header" style={{ height: "8vh"}}>
        <h5 className="modal-title">New Account</h5>
        <button type="button" className="close" data-dismiss="modal">
          <span aria-hidden="false">×</span>
        </button>
      </div>
      <div className="modal-body pt-30" style={{ height: "84vh",overflowY: "auto" }}>
        <div className="form-type-material">
          <Input
            name="userName"
            type="text"
            required={false}
            value={state.userName}
            error={state.errors.userName}
            onChange={handleChange}
            label="User name"/>
          <Select
            name="type"
            required={false}
            value={state.type}
            error={state.errors.type}
            onChange={handleChange}
            label="Account Type"
            options={mappedAccountTypes}/>
          <Input
            name="password"
            type="password"
            value={state.password}
            error={state.errors.password}
            onChange={handleChange}
            label="Password"/>
          <Input
            name="verifyPassword"
            className='do-float'
            type="password"
            value={state.verifyPassword}
            error={state.errors.verifyPassword}
            onChange={handleChange}
            label="Verify Password"/>
        </div>
      </div>
      <div className="modal-footer" style={{ height: "8vh"}}>
        <button type="button" className="btn btn-bold  btn-secondary" data-dismiss="modal">Cancel</button>
        {
           addError ?
           <SmartButton type='submit' title='Retry' isLoaoding={adding} className='btn-primary' disabled={adding} /> :
           <SmartButton type='submit' title='Save' isLoaoding={adding} className='btn-primary' disabled={adding} />
         }
      </div>
    </form>
  )
}
const Success = (props) => {
  const { clearform } = props;
  return(
    <section className="modal-content" >
      <div className="modal-body pt-30" style={style.center}>

        <div className="card p-60 text-center" style={{ height: "50%", overflowY: "none" }}>
          <h4 className="text-info text-uppercase">Account created</h4>
          <br/>
          <p>Account was successfully created</p>
          <br/>
          <br/>
          <button
            type="button"
            className="btn btn-bold btn-blockX btn-primary"
            data-dismiss="modal"
            onClick={clearform}>Done</button>
        </div>

      </div>
    </section>
  )
}
const style = {
  center: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    alignContent: 'center',
    height: "100%",
    overflow: "none"
  }
}
