import React, { Component } from 'react';
import Validator from '../../../../formValidations/FarmerEmptyDetails';
import withSuperForm from '../../../HOC/withSuperForm';
import SmartButton from '../../buttons/SmartButton';


// Components
import { Input, Select } from '../../formItems/';

// Location
const cities = [ 'Accra',  'Kumasi', 'Tema' ];
const initialState = {
  'details.firstName': '',
  'details.lastName': '',
  'details.nickName': '',
  'details.gender': '',
  'details.dob': '',
  'details.rfId': '',
  'details.email': '',
  'details.phone': '',
  'details.address': '',
  'details.city': '',
  errors: {}
};

const UI = ({ adding, added, error, state, handleChange, handleSubmit, clear }) => {
  const formatedCities = cities.map((city) => Object({ title: city, value: city }));

  return(
    <div className="min-h-fullscreen bg-img center-vh p-20" style={{backgroundImage: "url('/assets/img/bg/2.jpg')" }} data-overlay="7">
      <div className="card card-round card-shadowed px-50 py-30 w-600px mb-0" style={{maxWidth: "100%"}}>
        <h5 className="text-uppercase">Personal Information</h5>
        <br/>

        <form className="form-type-material" onSubmit={handleSubmit}>
          <div className='row'>
            <Input className='col-md-6' name="details.firstName" type="text" required={false}  error={state.errors['details.firstName']}  onChange={handleChange} label="First Name"/>
            <Input className='col-md-6' name="details.lastName" type="text" required={false}  error={state.errors['details.lastName']} onChange={handleChange} label="Last Name"/>
            <Input className='col-md-6' name="details.nickName" type="text" onChange={handleChange} label="Nick Name"/>
            <Input className='col-md-6' name="details.dob" className='do-float' type="date"  error={state.errors['details.dob']} onChange={handleChange} label="Date of birth"/>
            <Select className='col-md-6' name="details.gender" required={false}  error={state.errors['details.gender']} onChange={handleChange} label="Gender" options={[
              { title: "Male", value: "Male" },
              { title: "Female", value: "Female" }
            ]}/>
          </div>

          <div className="divider fs-16">Contact Information</div>
          <div className='row'>
            <Input className='col-md-6' name="details.email"  type="email" required={false} error={state.errors['details.email']}  onChange={handleChange} label="Email Address"/>
            <Input className='col-md-6' name="details.phone" type="tel" required={false}  error={state.errors['details.phone']}  onChange={handleChange} label="Phone Number"/>
            <Input className='col-md-12' name="details.address"  type="text"  onChange={handleChange} label="Address"/>
            <Select className='col-md-6' name="details.city" label="City" onChange={handleChange} options={formatedCities}/>

          </div>
          <p className="text-danger">{ error ? error.message : null }</p>

          <div className="form-group">
            {
               error ?
               <SmartButton type='submit' title='Retry' isLoaoding={adding} className='btn-primary btn-block' disabled={adding} /> :
               <SmartButton type='submit' title='Save' isLoaoding={adding} className='btn-primary btn-block' disabled={adding} />
            }
          </div>
        </form>
      </div>
    </div>
  )
}
export default withSuperForm(initialState, Validator)(UI);
