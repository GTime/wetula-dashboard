import React from 'react';
import withUpdateForm from '../../../HOC/withUpdateForm'
import { Input, Select } from '../../formItems/'
import SmartButton from '../../buttons/SmartButton';

// Location

const UI = (props) => {
  const { form, detail, updating, updated, error } = props;

  return(
    <form data-selected={detail.id} onSubmit={form.handleSubmit} className='card' >
      <div className='card-body bb-1 border-fade'>
        <h4 className=' opacity-90'><strong>Account Information</strong></h4>

        <div className='form-type-material'><br/>
          <Input
            className='do-float'
            name='userName'
            type='text'
            onChange={form.handleChange}
            defaultValue={detail.userName}
            label='User name'/>

          <div className='col-md-12'>
            {
              error ?
              <SmartButton type='submit' title='Retry' isLoaoding={updating} className=' btn-block btn-primary' /> :
              <SmartButton type='submit' title='Update' isLoaoding={updating}  className=' btn-block btn-primary' disabled={!form.enableUpdate}/>
            }
          </div>
        </div>
      </div>
    </form>
  )
}
export default withUpdateForm(UI)
