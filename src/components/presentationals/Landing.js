import React from 'react';


export default () => {
	const style = {
		display: "flex",
		flexDirection: "column",
		justifyContent: "center",
		alignItems: "center"
	}
	return (
		<section id="container" >
			<section className="wrapper" style={style}>

					<div className="col-lg-8" style={{position: "absolute", top: "30%"}}>
							<Panel title="User" link="/user"/>
							<Panel title="Staff" link="/staff"/>
							<Panel title="Admin" link="/admin"/>
					</div>

	  	</section>
	  </section>
	);
}

const Panel = (props) => {
	const { title, link} = props;
	return(
		 <div className="col-md-4 col-sm-12 col-xs-12 mb">
			 <a href={link}>
				 <div className="weather pn">
					 <i className="fa fa-user fa-4x"></i>
					 <h2>{title}</h2>
				 </div>
			 </a>
		 </div>
	);
}
