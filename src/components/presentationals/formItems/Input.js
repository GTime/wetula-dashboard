import React from 'react';
import prototypes  from 'prop-types';

//Input
const Input = ({ className, disabled, name, type, onChange, error, label, placeholder, defaultValue, value, required}) => {

	return (
		<div className={"form-group " + className}>
			<input
				className="form-control "
				name={name}
				type={type}
				onChange={onChange}
				placeholder={placeholder}
				defaultValue={defaultValue}
				disabled={disabled}
				required={required}
				value={value}
			/>
			<label>{label}</label>
			<p className='text-danger'>{error}</p>
		</div>
		);
};
export default  Input;

Input.prototypes = {
	className: prototypes.string,
	name: prototypes.string.isRequired,
	type: prototypes.string.isRequired,
	onChange: prototypes.func.isRequired,
	defaultValue: prototypes.string,
	error: prototypes.string,
	label: prototypes.string,
	value: prototypes.string.isRequired,
	required: prototypes.boolean,
	disabled: prototypes.boolean
}
Input.prototypes = {
	type: 'text'
}
