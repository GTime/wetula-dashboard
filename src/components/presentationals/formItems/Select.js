import React from 'react';
import prototypes  from 'prop-types';

//Field
const Select = ({field, className, name, onChange, label, error, defaultValue, options, required }) => {
	const newOptions = defaultValue ? [ {title: defaultValue, value: defaultValue }, ...options ] : [ {title: 'None', value: ''}, ...options ];
  const allOptions = newOptions.map(
    (option, key) => <option key={key} value={option.value}>{ option.title}</option>
  );
	return (
		<div className={"form-group do-float " + className}>
        <select
					name={name}
					onChange={onChange}
					className="form-control"
					required={required}
					defaultValue={defaultValue}
				>
          { allOptions }
        </select>
				<label>{label}</label>
				<p className='text-danger'>{error}</p>
    </div>
  )
};
export default Select;

Select.prototypes = {
  label: prototypes.string,
	error: prototypes.string,
	defaultValue: prototypes.string,
	name: prototypes.string.isRequired,
	onChange: prototypes.func.isRequired,
  options: prototypes.array.isRequired,
	required: prototypes.boolean
}
