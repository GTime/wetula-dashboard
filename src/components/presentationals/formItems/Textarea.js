import React from 'react';
import prototypes  from 'prop-types';


//Textarea
const Textarea = ({field, name, type, onChange, error, label, value, required}) => {
	// const style = { backgroundImage: "linear-gradient(#faa64b, #faa64b),linear-gradient(#ebebeb, #ebebeb)" }
	const style = {  }
	return (
		<div className="form-group">
			<textarea
				className="form-control"
				name={name}
				onChange={onChange}
				value={value}
				style={style}
				required={required}
			> </textarea>
			<label>{label}</label>
		</div>
	);
};

export default Textarea;

Textarea.prototypes = {
	name: prototypes.string.isRequired,
	onChange: prototypes.func.isRequired,
	error: prototypes.string,
	label: prototypes.string.isRequired,
	value: prototypes.string.isRequired,
	required: prototypes.boolean
}
