import React from 'react';
import prototypes  from 'prop-types';

//Input
const File = ({field, name, onChange, error, label }) => {
	// const style = { backgroundImage: "linear-gradient(#faa64b, #faa64b),linear-gradient(#ebebeb, #ebebeb)" }
	const style = {  }
	return (
		<div className="form-group form-type-material file-group">
			<input
				className="form-control file-value file-browser"
				type="text"
				style={style}
				readOnly
			/>
			<label>{label}</label>
			<input name={name} onChange={onChange} type="file" multiple/>
		</div>
		);
};
export default  File;

File.prototypes = {
	name: prototypes.string.isRequired,
	onChange: prototypes.func.isRequired,
	error: prototypes.string,
	label: prototypes.string.isRequired
}
