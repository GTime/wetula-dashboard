import React, { Component } from 'react';


export default (props) => {
	const { searchProduct  } = props;
	return (
		<header className="flexbox align-items-center media-list-header bg-transparent b-0 py-16 pl-20">
			<div className="flexbox align-items-center">
				{/* <div className="custom-control custom-checkbox">
					<input type="checkbox" className="custom-control-input"/>
					<label className="custom-control-label"></label>
				</div> */}

				<div className="dropdown d-none d-sm-block">
					<a className="btn btn-sm dropdown-toggle" data-toggle="dropdown" href="#">Sort by</a>
					<div className="dropdown-menu">
						<a className="dropdown-item" href="#">Date</a>
						<a className="dropdown-item" href="#">Name</a>
						<a className="dropdown-item" href="#">Expired</a>
					</div>
				</div>
			</div>

			<div>
				<div className="lookup lookup-circle lookup-right">
					<input type="text" data-provide="media-search" onChange={searchProduct}/>
				</div>
			</div>
		</header>
	);
}
