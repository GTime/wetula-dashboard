
import React, { Component } from 'react';

// Components
import LinearPreloader from '../../extra/LinearPreloader'

export default (props) => {
  const { clearform, handleToggleDelete, deleteProduct, actions } = props;
  return(
      <div className="card p-60 text-center" style={{ height: "50%", overflowY: "none" }}>
        <h4 className="text-danger text-uppercase">Delete Product</h4>
        { deleteProduct.deletingProduct ? <h3 className="price text-info"><LinearPreloader/></h3> : null}
        { (deleteProduct.deletedProduct || deleteProduct.deletingProduct)  ? null  : <ActionButtons {...props}/>  }
        { deleteProduct.deletedProduct ? <DeleteSuccess handleToggleDelete={handleToggleDelete}/> : null }
      </div>
		)
}

const ActionButtons = (props) => {
  const { handleToggleDelete, clearform, deleteProduct, actions, detail } = props;
  return(
    <div>
      <p>Are you really sure want to delete medicine?</p>
      <br/>
      <button type="button" className="btn btn-bold btn-primary" data-dismiss="modal" onClick={handleToggleDelete} style={{ marginRight: '5px' }}>No</button>
      <button data-selected={detail.id} type="button" className="btn btn-bold btn-danger" onClick={actions.deleteProduct}>Yes</button>
    </div>
  )
}

// Success
export const DeleteSuccess = (props) => {
  const { handleToggleDelete } = props;
  return(
      <div>
        <h3 className=" text-info">Deleted</h3>
        <br/>
        <button
          type="button"
          className="btn btn-bold btn-blockX btn-primary"
          data-dismiss="modal"
          onClick={handleToggleDelete}>Done</button>
      </div>
		)
}
