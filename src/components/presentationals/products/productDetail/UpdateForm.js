
import React from 'react';

// Components
import { Input, Select } from '../../formItems/';
import { isEmpty } from '../../../tools/utilities';

// Location
const regions = [ 'Greater Accra',  'Central Region', 'Volta Region' ];
const cities = [ 'Accra',  'Kumasi', 'Tema' ];



// Form
export const Form = (props) => {
  const { groups, staffGroups,  formatedCities, formatedRegions, handleChange, detail } = props;

  return(
    <div className="form-type-material" style={{ padding: "1rem" }}>

      <Input name="name" type="text" className=' do-float'  defaultValue={detail.name}   onChange={handleChange} label="Name"/>
      <Input name="manCompany" type="text" className=' do-float'  required={false}  defaultValue={detail.manCompany}   onChange={handleChange} label="Manufacture Company"/>
      <Input name="countryOfOrigin" type="text" className=' do-float'  defaultValue={detail.countryOfOrigin}  onChange={handleChange} label="Country of Origin"/>
      <Input name="boxQuantity" type="number" className=' do-float'  defaultValue={detail.boxQuantity}   onChange={handleChange} label="Box Quantity"/>
      <Input name="quantityInStock" type="number" className=' do-float'  defaultValue={detail.quantityInStock}   onChange={handleChange} label="Quantity in Stock"/>
      <Input name="sellingPrice" type="number" className=' do-float'  defaultValue={detail.sellingPrice} onChange={handleChange} label="Selling Price"/>
      <Select name="category" className=' do-float'  defaultValue={detail.category}  onChange={handleChange} label="Category" options={[
        {title: 'Syrup', value: 'Syrup'},
        {title: 'Tablet', value: 'Tablet'},
        {title: 'Capsule', value: 'Capsule'},
        {title: 'Others', value: 'Others'}
      ]}/>
      <Input name="supplier" type="text" className=' do-float'  defaultValue={detail.supplier} onChange={handleChange} label="Supplier"/>
      <Input name="manDate" type="date" className=' do-float'  defaultValue={detail.manDate} onChange={handleChange} label="Manufacture Date"/>
      <Input name="expDate" type="date" className=' do-float'  defaultValue={detail.expDate} onChange={handleChange} label="Expiry Date"/>
    </div>
  )
}

// Success
export const Success = (props) => {
  const { clearform } = props;
  return(
      <div className="card p-60 text-center" style={{ height: "50%", overflowY: "none" }}>
        <h4 className="text-info text-uppercase">Staff Updated</h4>
        <br/>
        <p>Staff was successfully updated to staffs</p>
        <br/>

        <h3 className="price text-info"></h3>

        <br/>
        <button
          type="button"
          className="btn btn-bold btn-blockX btn-primary"
          data-dismiss="modal"
          onClick={clearform}>Done</button>
      </div>
		)
}
