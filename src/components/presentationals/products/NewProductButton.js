import React, { Component } from 'react';


export default () => {
	return (
		<div className="fab fab-fixed">
      <a className="btn btn-float btn-primary" href="#modal-add-product" title="New Product" data-toggle="modal" data-target="#modal-add-product">
				<i className="ti-plus"></i>
			</a>
    </div>
	);
}
