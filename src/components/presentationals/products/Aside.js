import React, { Component } from 'react'
import withLoader from '../../HOC/withLoader'


export default (props) => {
	const {  } = props;
	return(
		<aside className="aside aside-expand-md">
			<div className="aside-body">
				<ul className="nav nav-lg nav-pills flex-column">
					<li className="nav-item active">
						<i className="fa fa-users"></i>
						<a className="nav-link" href="#">All</a>
					</li>

					<li className="nav-item">
						<i className="fa fa-user"></i>
						<a className="nav-link" href="#">New</a>
						{/* <span className="badge badge-pill badge-default">6</span> */}
					</li>

					<li className="nav-item">
						<i className="fa fa-user-times"></i>
						<a className="nav-link" href="#">Expired</a>
					</li>
				</ul>
				<hr/>
				{/* <Groups groups={ groups }/> */}

			</div>
			<button className="aside-toggler"></button>
		</aside>
	)
}
