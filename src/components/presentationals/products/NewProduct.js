import React, { Component } from 'react';
import Validator from '../../formValidations/NewProductVal';

// Components
import { Input, Select } from '../formItems/'

// Location
const regions = [ 'Greater Accra',  'Central Region', 'Volta Region' ];
const cities = [ 'Accra',  'Kumasi', 'Tema' ];
const initialState = {
  name: '',
  manCompany: '',
  countryOfOrigin: '',
  boxQuantity: '',
  quantityInStock: '',
  sellingPrice: '',
  category: '',
  supplier: '',
  manDate: '',
  expDate: '',
  errors: {}
};

export default class extends Component {
  constructor(props) {
    super(props);
    this.state = initialState;
		this.handleChange = this.handleChange.bind(this);
		this.handleSubmit = this.handleSubmit.bind(this);
    this.handleClearForm = this.clearForm.bind(this);
  }
  isValid(){
    const { errors, isValid } = Validator(this.state);
    if(!isValid){
      this.setState({ errors })
    }
    return isValid
  }
  clearForm(){
    this.setState(initialState);
    this.props.actions.reset();
  }
	handleChange(e) {
		this.setState({ [e.target.name]:  e.target.value });
	}
	handleSubmit(e ) {
		e.preventDefault();
    this.isValid()

    if(this.isValid()){
      delete this.state.errors;
      const data = this.state;
      this.setState({ errors: {} });
      this.props.actions.addProduct(this.state)
    }
	}
  render () {
    const { addingProduct, addedProduct, addProductrror } = this.props;
    const formProps = {
      state: this.state,
      handleChange: this.handleChange,
      handleSubmit: this.handleSubmit,
      addingProduct, addProductrror
    };

    return(
      <div className="modal modal-right fade" id="modal-add-product" tabIndex="-1">
        <div className="modal-dialog modal-sm">
          { addedProduct ? <Success clearform={this.handleClearForm}/>  :  <Form { ...formProps }/>  }
        </div>
      </div>
    )
  }
}
const SmartButton = ({ type, title, isLoaoding, color,  disabled= false }) => {
  return <button
    type={type}
    className={'btn btn-bold  ' + color }
    disabled={isLoaoding || disabled ? true : false}>
    { isLoaoding ? 'Loading...' : title}
  </button>
}

// Form
const Form = (props) => {
  const {
    addProductrror, addingProduct,
    handleSubmit, handleChange, state
  } = props;

  return(
    <form onSubmit={handleSubmit} className="modal-content" >

      <div className="modal-header" style={{ height: "8vh"}}>
          <h5 className="modal-title">New Product</h5>
          <button type="button" className="close" data-dismiss="modal">
            <span aria-hidden="false">×</span>
          </button>
        </div>
      <div className="modal-body pt-30" style={{ height: "84vh",overflowY: "auto" }}>
        <div className="form-type-material">

          <Input name="name" type="text" value={state.name} error={state.errors.name}  onChange={handleChange} label="Name"/>
          <Input name="manCompany" type="text" required={false}  value={state.manCompany} error={state.errors.manCompany}  onChange={handleChange} label="Manufacture Company"/>
          <Input name="countryOfOrigin" type="text" value={state.countryOfOrigin} error={state.errors.countryOfOrigin} onChange={handleChange} label="Country of Origin"/>
          <Input name="boxQuantity" type="number" value={state.boxQuantity}  error={state.errors.boxQuantity} onChange={handleChange} label="Box Quantity"/>
          <Input name="quantityInStock" type="number" value={state.quantityInStock}  error={state.errors.quantityInStock} onChange={handleChange} label="Quantity in Stock"/>
          <Input name="sellingPrice" type="number" value={state.sellingPrice} error={state.errors.sellingPrice} onChange={handleChange} label="Selling Price"/>
          <Select name="category" value={state.category} error={state.errors.category} onChange={handleChange} label="Category" options={[
            {title: 'Syrup', value: 'Syrup'},
            {title: 'Tablet', value: 'Tablet'},
            {title: 'Capsule', value: 'Capsule'},
            {title: 'Others', value: 'Others'}
          ]}/>
          <Input name="supplier" type="text" className=' do-float'  value={state.supplier} error={state.errors.supplier} onChange={handleChange} label="Supplier"/>
          <Input name="manDate" type="date" className=' do-float'  value={state.manDate} error={state.errors.manDate} onChange={handleChange} label="Manufacture Date"/>
          <Input name="expDate" type="date" className=' do-float'  value={state.expDate} error={state.errors.expDate} onChange={handleChange} label="Expiry Date"/>
        </div>
      </div>

      <div className="modal-footer" style={{ height: "8vh"}}>
        <button type="button" className="btn btn-bold  btn-secondary" data-dismiss="modal">Cancel</button>
        {
           addProductrror ?
           <SmartButton type='submit' title='Retry' isLoaoding={addingProduct} color='btn-primary' disabled={false} /> :
           <SmartButton type='submit' title='Register Product' isLoaoding={addingProduct} color='btn-primary' disabled={false} />
      }
      </div>

    </form>
  )
}

// Success
const Success = (props) => {
  const { clearform } = props;
  const style = {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    alignContent: 'center',
    height: "100%",
    overflow: "none"
  }
  return(
    <section className="modal-content" >
      <div className="modal-body pt-30" style={style}>

        <div className="card p-60 text-center" style={{ height: "50%", overflowY: "none" }}>
          <h4 className="text-info text-uppercase">Product Added</h4>
          <br/>
          <p>Product was successfully added</p>
          <br/>

          <h3 className="price text-info"></h3>

          <br/>
          <button
            type="button"
            className="btn btn-bold btn-blockX btn-primary"
            data-dismiss="modal"
            onClick={clearform}>Done</button>
        </div>

      </div>
    </section>
  )
}
