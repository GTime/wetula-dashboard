import React from 'react';
import Product from './Product';
import BundledProducts from './BundledProducts'
import Pagination from '../extra/Pagination'

export default (props) => {
	return(
		<div className="main-content">
			<div className="media-list media-list-divided media-list-hover" data-provide="selectall">
				<BundledProducts { ...props }/>
			</div>
		</div>
	)
}
