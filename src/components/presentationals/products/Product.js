import React from 'react';

export default (props) => {
	const { product,  actions } = props;
	const { id, name, quantityInStock, sellingPrice } = product;
	const { select, selectProduct } = actions;

	return (
		<div className="media align-items-center">
			<div className="custom-control custom-checkbox" >
				<input data-selected={id} type="checkbox"  className="custom-control-input" onChange={select}/>
				<label className="custom-control-label"></label>
			</div>

			<a data-selected={id} onClick={selectProduct} className="flexbox flex-grow gap-items text-truncate" href="#modal-product-details" data-toggle="modal">
				<div data-selected={id} className="media-body text-truncate">
					<p data-selected={id}>
						<a data-selected={id} className="hover-primary" role="button">
							<strong data-selected={id}>{name}</strong>
						</a>
						<small data-selected={id} className="sidetitle">{quantityInStock} Left</small>
					</p>
					<p data-selected={id}>GHc{sellingPrice}</p>
				</div>
			</a>

			{/* <div className="media-right gap-items">
				<a className="lead btn btn-primary" role="button" title="Delete Product" data-selected={id}>
					<i className="ti-trash" data-selected={id}></i>
				</a>
			</div> */}
		</div>
	);
}
