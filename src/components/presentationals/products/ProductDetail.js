
import React, { Component } from 'react';
import Validator from '../../formValidations/NewProductVal';
import { isEmpty } from '../../tools/utilities';

// Components
import { Input, Select } from '../formItems/'
import LinearPreloader from '../extra/LinearPreloader'
import Delete, { DeleteSuccess} from './productDetail/Delete'
import { Form, Success } from './productDetail/UpdateForm'


export default class extends Component {
  constructor(props) {
    super(props);
    this.state = {
      openDelete: false
    };
		this.handleChange = this.handleChange.bind(this);
		this.handleSubmit = this.handleSubmit.bind(this);
    this.handleClearForm = this.clearForm.bind(this);
    this.handleToggleDelete = this.handleToggleDelete.bind(this);
  }
  clean(){
    for (var key in this.state) {
      if ( isEmpty(this.state[key]) ||  key == 'enableUpdate' || key === 'openDelete'){
        delete this.state[key]
      }
    }
  }
  handleToggleDelete(){
    this.setState((prevState) => ({
      openDelete: !prevState.openDelete
    }))
    this.props.actions.reset();
  }
  clearForm(){
    this.setState({ enableUpdate: false })
    this.props.actions.reset();
  }
	handleChange(e) {
    if (!isEmpty(e.target.value)) {
      this.setState({ enableUpdate: true })
    }
		this.setState({ [e.target.name]:  e.target.value });
    this.clean();
	}
	handleSubmit(e ) {
		e.preventDefault();
    this.clean();
    if (!isEmpty(this.state)) {
      const { id } = this.props.detail;
      this.props.actions.updateProduct(id, this.state);
    }
	}
  render () {
    const { detail, updateProduct, deleteProduct , actions} = this.props;
  	const handles = {
  		handleChange: this.handleChange,
  		handleClearForm: this.handleClearForm,
  		handleToggleDelete: this.handleToggleDelete
  	}
    return(
      <div className="modal modal-right fade" id="modal-product-details" tabIndex="-1">
				<div className="modal-dialog modal-sm">
          <form onSubmit={this.handleSubmit} className="modal-content" >
            <div className="modal-header">
              Product
            </div>
            { !isEmpty(detail) ? <Body {...this.state} {...this.props} { ...handles }/> : <LinearPreloader/> }
            { !isEmpty(detail) ? <Footer
              updateProduct={updateProduct}
              openDelete={this.handleToggleDelete}
              enableUpdate={this.state.enableUpdate}/> : null }
          </form>
          </div>
      </div>
    )
  }
}

// Modal Body
const Body = (props) => {
  return(
    <div className="modal-body" style={{ height: "84vh",overflowY: "auto", padding: "0px" }}>
      { props.openDelete ? <DeleteContainer { ...props } /> : <FormContainer  { ...props }/> }
    </div>
  )
}

// Extra
const SmartButton = ({ type, title, isLoaoding, color,  disabled= false, onClick }) => {
  return <button
    type={type}
    className={'btn btn-bold  ' + color }
    disabled={isLoaoding || disabled ? true : false}>
    { isLoaoding ? 'Loading...' : title}
  </button>
}

// Update Form and Delete View Container
const FormContainer = (props) => {
  const { openDelete, updatedProduct,  clearform } = props;
  return updatedProduct && !openDelete ? <Success clearform={clearform}/>  : <Form { ...props }/>
}
const DeleteContainer = (props) => {
  const { handleToggleDelete, clearform, handleCloseDelete, deletedProduct, openDelete, actions, productId } = props;

  if (deletedProduct && !openDelete) {
    return <DeleteSuccess handleToggleDelete={handleToggleDelete}/>
  }
  return  <Delete {...props}/>
}

// Modal Footer
const Footer = (props) => {
  const { updateProduct, openDelete, enableUpdate  } = props
  return(
    <div className="modal-footer" style={{ height: "8vh"}}>
      <button type="button" className="btn btn-bold  btn-secondary" data-dismiss="modal">Cancel</button>
      {
        updateProduct.updateProductError ?
        <SmartButton type='submit' title='Retry' isLoaoding={updateProduct.updatingProduct} color='btn-primary' /> :
        <SmartButton type='submit' title='Update' isLoaoding={updateProduct.updatingProduct} color='btn-primary' disabled={!enableUpdate} />
      }

      <button type="button" className="btn btn-bold  btn-danger" onClick={openDelete}>Delete</button>
    </div>
  )
}
