import React, {Component } from 'react';
import withLoader from '../../HOC/withLoader';
import Product from './Product';

class BundledProduct extends Component {
	render(){
		const { products, groups,  actions } = this.props;
		const allProduct = products.map((product, key) =>  <Product key={key} product={product}  actions={actions}/>);

		return(
			<div className="media-list-body bg-white b-1">
				{allProduct}
			</div>
		)
	}
}

export default withLoader('products')(BundledProduct, "Redux Action");
