import React from 'react';

export default () => <nav className="mt-3">
	<ul className="pagination justify-content-center">
		<li className="page-item disabled">
			<a className="page-link" href="#">
				<span className="ti-arrow-left"></span>
			</a>
		</li>
		<li className="page-item active"><a className="page-link" href="#">1</a></li>
		<li className="page-item"><a className="page-link" href="#">2</a></li>
		<li className="page-item"><a className="page-link" href="#">3</a></li>
		<li className="page-item"><a className="page-link" href="#">4</a></li>
		<li className="page-item"><a className="page-link" href="#">5</a></li>
		<li className="page-item">
			<a className="page-link" href="#">
				<span className="ti-arrow-right"></span>
			</a>
		</li>
	</ul>
</nav>
