import React from 'react';

//  LinearPreloader
export default ({ type, title, isLoaoding, className, disabled = false, onClick}) => {
  return <button
    type={type}
    className={'btn ' + className }
    disabled={isLoaoding || disabled ? true : false}
    onClick={ onClick ? onClick : null }
  >
    { isLoaoding ? 'Loading...' : title}
  </button>
}
