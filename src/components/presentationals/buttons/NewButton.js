import React, { Component } from 'react';


export default ({ title, target, onClick }) => {
	return (
		<div className="fab fab-fixed">
      <button
				onClick={onClick}
				className="btn btn-float btn-primary"
				href={target}
				title={title}
				data-toggle="modal"
				data-target={target}>
				<i className="ti-plus"></i>
			</button>
    </div>
	);
}
