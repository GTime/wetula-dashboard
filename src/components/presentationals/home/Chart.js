import React from 'react';
import {
	LineChart,
	CartesianGrid,
	XAxis,
	YAxis,
	Tooltip,
	Legend,
	Line,
	ResponsiveContainer
} from 'recharts';

// Chart
export default (props) => {
	return(
		<div className="col-md-12">
        <div className="card">
					<div className="card-header">
            <h5 className="card-title"><strong>{props.title}</strong></h5>
            {
							props.link ?
							<a
								className="btn btn-sm btn-secondary"
								href={props.link.path}>
									{props.link.title}
							</a> :
							null
						}
          </div>

          <div className="card-body">
						<ResponsiveContainer width={props.width} height={props.height} >
							<LineChart data={props.data}
								margin={{ top: 5, right: 30, left: 20, bottom: 5 }}>
								<CartesianGrid strokeDasharray="3 3" />
								<XAxis dataKey="name" />
								<YAxis />
								<Tooltip />
								<Legend />
								<Line type="monotone" dataKey="Sales" stroke="#8884d8" />
								{/* <Line type="monotone" dataKey="Expired" stroke="#82ca9d" /> */}
								{/* <Line type="monotone" dataKey="vegra" stroke="#000000" /> */}
								{/* <Line type="monotone" dataKey="Lonart" stroke="#333333" /> */}
							</LineChart>
						</ResponsiveContainer>

          </div>
        </div>
      </div>
	)
}

const style = {
	cardBody: {
		position: 'relative'
	}
}
