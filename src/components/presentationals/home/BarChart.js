import React from 'react';
import {
	BarChart,
	CartesianGrid,
	XAxis,
	YAxis,
	Tooltip,
	Legend,
	Bar,
	ResponsiveContainer
} from 'recharts';

// Chart
export default (props) => {
	return(
		<div className="col-md-6">
        <div className="card">
          <div className="card-header">
            <h5 className="card-title"><strong>{props.title}</strong></h5>
            {
							props.link ?
							<a
								className="btn btn-sm btn-secondary"
								href={props.link.path}>
									{props.link.title}
							</a> :
							null
						}
          </div>

          <div className="card-body" style={style.cardBody}>
						<ResponsiveContainer width={props.width} height={props.height}>
							<BarChart  data={props.data}
								margin={{ top: 5, right: 30, left: 20, bottom: 5 }}>
								<CartesianGrid strokeDasharray="3 3" />
								<XAxis dataKey="name" />
								<YAxis />
								<Tooltip />
								<Legend />
								<Bar dataKey="Iching" fill="#8884d8" />
								<Bar dataKey="Headeches" fill="#82ca9d" />
								<Bar dataKey="Pain" fill="#000000" />
								<Bar dataKey="Weakness" fill="#333333" />
							</BarChart>
						</ResponsiveContainer>
          </div>
        </div>
      </div>
	)
}

const style = {
	cardBody: {
		position: 'relative'
	}
}
