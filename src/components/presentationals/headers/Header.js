import React  from 'react';

export default ({ title, subtitle, handleSearch, backgroundImage }) => {
	const bgImage = backgroundImage? backgroundImage : '/assets/img/gallery/1.jpg';

	return(
		<header
			className="header header-inverse bg-img"
			style={{backgroundImage: "url('" + bgImage + "')"}}
			data-overlay="7">
          <div className="header-info">
            <div className="left">
              <h2 className="header-title">
								<strong>{title}</strong>
								<small className="subtitle">{subtitle}</small>
							</h2>
            </div>

						{
							handleSearch?
							<div className="right flex-middle">
	              <form className="lookup lookup-circle lookup-lg lookup-right" target="index.html">
	                <input type="text" name="s" onChange={handleSearch}/>
	              </form>
	            </div>:
							null
						}
          </div>
        </header>
	)
}
