import React, { Component } from 'react';
import { connect } from 'react-redux';
import { fetchFarmers } from '../actions/farmers';
import { addAccount, resetAccount } from '../actions/accounts';
import { isEmpty } from '../tools/util';

import withDashboard from './HOC/withDashboard';
import Header from './presentationals/headers/Header';
import NewButton from './presentationals/buttons/NewButton';
import Pagination from './presentationals/Pagination';
import Farmer from './presentationals/accounts/Farmer';
import New from './presentationals/accounts/forms/NewAccount';
import Preloader from './presentationals/extra/LinearPreloader';
import NoData from './presentationals/extra/NoData';


class UI extends Component {
	componentDidMount() {
		this.props.fetchFarmers();
	}
	render() {
		const { farmers, fetching, add, addAccount, reset, fetchFarmers } = this.props;
		const formated = farmers.map((farmer, key) => <Farmer key={key} { ...farmer }/>);

		return (
			<main className="main-container">
				<Header title='Farmers' subtitle='Explore all farmers'/>
				<section className="main-content">
					<div className="card">
						<Preloader active={fetching}/>
						{
							isEmpty(farmers)?
							<NoData message='No farmer recorded yet'/>:
							<div className="media-list media-list-divided media-list-hover">
								{ formated }
								{/* <Pagination/> */}
							</div>
						}
					</div>
				</section>
				<NewButton title='Add New Farmer' target='#modal-new-farmer'/>
				<New
					add={addAccount}
					reset={reset}
					{ ...add }/>
			</main>
		)
	}
}

const mapStateToProps = ({ farmers, accounts }) => {
	return {
		farmers: farmers.farmers,
		fetching: farmers.fetching,
		fetched: farmers.fetched,
		error: farmers.error,
		add: {
			adding: accounts.adding,
			added: accounts.added,
			error:accounts.addError
		}
	}
}
const mapDispatchToProp = (dispatch) => ({
	fetchFarmers: () => dispatch(fetchFarmers()),
	addAccount: (data) => {
		dispatch(addAccount(data));
		dispatch(fetchFarmers());
	},
	reset: () => {
		dispatch(addAccount());
		dispatch(fetchFarmers())
	},
})
const Conneted = connect(mapStateToProps, mapDispatchToProp)(UI);
export default withDashboard(Conneted);
