import React, { Component } from 'react'
import { connect } from 'react-redux'


// HOC
import withDashboard from '../HOC/withDashboard';
import { isEmpty } from '../tools/utilities';

// Props Setups
import { mapStateToProps, mapDispatchToProps } from  '../props/profileProps'

// Components
import LinearPreloader from '../components/extra/LinearPreloader'
import DeleteSuccess from  '../components/extra/DeleteSuccess'
import SwitchView from  '../components/extra/SwitchView'
import Header from  '../components/profile/Header'
import Details from  '../components/profile/Details'
import Activities from  '../components/profile/Activities'
import Settings from  '../components/profile/Settings'


class profile extends Component {
	constructor(props){
		super(props);
		this.state = {
			activeLink: 'details'
		};
		this.changeView = this.changeView.bind(this);
	}
	componentWillMount(){
		this.props.actions.profile.fetchProfile(this.props.match.params.id)
	}
	changeView(e){
		const { link } = e.target.dataset;
		if (link) {
			this.setState({ activeLink: link})
		}
	}
	render(){
		const { profile, settings, user } = this.props.states;
		const { actions } = this.props;
		let headerProps = {
			nav:{
				activeLink: this.state.activeLink,
				links: [
					{title: 'Details', link: 'details'},
					// {title: 'Activities', link: 'activities'},
					{title: 'Settings', link: 'settings'}
				],
				changeView: this.changeView
			},
			profile: profile.profile
		}
		let detailsProps = {
			profile: profile.profile
		}
		let settingsProps = {
			user: user,
			profile: profile.profile,
			...settings,
			actions: actions.settings
		}
		let switchView = {
			activeLink: this.state.activeLink,
			views: [
				{link: 'details', component: Details, props: detailsProps},
				{link: 'activities', component: Activities, props: profile.profile},
				{link: 'settings', component: Settings, props: settingsProps}
			]
		}

		if (settings.delete.deleted) {
			return <DeleteSuccess link='/staff' title=" Staffs"/>
		}

		if (profile.fetching) {
			return (
				<main className="main-container">
					<div className="main-content">
						 <LinearPreloader/>
					</div>
					{this.props.footer}
				</main>
			)
		}
		else if (profile.fetched || !isEmpty(profile.profile) ) {
			return (
				<main className="main-container">
					<Header {...headerProps}/>
					<div className="main-content">
						 <SwitchView {...switchView}/>
					</div>
					{this.props.footer}
				</main>
			)
		}
		else {
			return (
				<main className="main-container">
					<div className="main-content">
						 <h1>An error occured</h1>
					</div>
					{this.props.footer}
				</main>
			)
		}


	}
}

const MappedComponent = connect(mapStateToProps, mapDispatchToProps)(profile);
export default withDashboard(MappedComponent);
