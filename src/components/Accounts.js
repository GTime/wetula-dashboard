import React, { Component } from 'react';
import { connect } from 'react-redux';
import { fetchAccounts, addAccount, resetAccount } from '../actions/accounts';

import withDashboard from './HOC/withDashboard';
import Header from './presentationals/headers/Header';
import NewButton from './presentationals/buttons/NewButton';
import Pagination from './presentationals/Pagination';
import Account from './presentationals/accounts/Account';
import New from './presentationals/accounts/New';
import Preloader from './presentationals/extra/LinearPreloader';


class UI extends Component {
	componentDidMount() {
		this.props.fetchAccounts();
	}
	render() {
		const { accounts, fetching, add, addAccount, reset, fetchAccounts } = this.props;
		const formated = accounts.map((account, key) => <Account key={key} { ...account }/>);

		return <main className="main-container">
			<Header title='Accounts' subtitle='Explore all accounts'/>
			<Preloader active={fetching}/>
			<section className="main-content">
				<div className="card">
					<div className="media-list media-list-divided media-list-hover">
						{ formated }
						<Pagination/>
					</div>
				</div>
			</section>
			<NewButton title='Add New Account' target='#modal-new-account'/>
			<New
				add={addAccount}
				reset={reset}
				{ ...add }
			/>
		</main>
	}
}

const mapStateToProps = ({ accounts }) => {
	return {
		// accounts: fakeAccounts,
		accounts: accounts.accounts,
		fetching: accounts.fetching,
		fetched: accounts.fetched,
		error: accounts.error,
		add: {
			adding: accounts.adding,
			added: accounts.added,
			error:accounts.addError
		}
	}
}
const mapDispatchToProp = (dispatch) => ({
	fetchAccounts: () => dispatch(fetchAccounts()),
	addAccount: (data) => {
		dispatch(addAccount(data));
		dispatch(fetchAccounts());
	},
	reset: () => {
		dispatch(resetAccount());
		dispatch(fetchAccounts())
	},
})
const Conneted = connect(mapStateToProps, mapDispatchToProp)(UI);
export default withDashboard(Conneted);
