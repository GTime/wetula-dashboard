import React, { Component } from 'react';
import { connect } from 'react-redux'
import Validator from '../formValidations/login';
import { loginAccount } from '../actions/accounts';
import { Input, Select } from './presentationals/formItems/';
import SmartButton from './presentationals/buttons/SmartButton';

const initialState = {
  userName: '',
  password: '',
  errors: {}
};
class Login extends Component {
  constructor(props) {
    super(props);
    this.state = initialState;
		this.handleChange = this.handleChange.bind(this);
		this.handleSubmit = this.handleSubmit.bind(this);
  }
  componentWillMount(){
    if(this.props.authenticated){
      this.props.history.replace('/farmers')
    }
  }
  componentWillUpdate(nextProps){
    if(nextProps.authenticated){
      this.props.history.replace('/farmers')
    }
  }
  isValid(){
    const { errors, isValid } = Validator(this.state);
    if(!isValid){
      this.setState({ errors })
    }
    return isValid
  }
	handleChange(e) {
		this.setState({ [e.target.name]:  e.target.value });
	}
	handleSubmit(e ) {
		e.preventDefault();
    this.isValid()

    if(this.isValid()){
      delete this.state.errors;
      const data = this.state;
      this.setState({ errors: {} });
      this.props.login(this.state)
    }
	}
  render () {
		const { userName, password, errors } = this.state;
		const { authenticating, error } = this.props;

		return (
			<div className="min-h-fullscreen bg-img center-vh p-20" style={{backgroundImage: "url('/assets/img/bg/2.jpg')" }} data-overlay="7">
				<div className="card card-round card-shadowed px-50 py-30 w-400px mb-0" style={{maxWidth: "100%"}}>
		      <h5 className="text-uppercase">Sign in</h5>
		      <br/>

          <form className="form-type-material" onSubmit={this.handleSubmit}>
						<Input
              name="userName"
              type="text"
              required={false}
              value={userName}
              error={errors.userName}
              onChange={this.handleChange}
              label="Email"/>
	          <Input
              name="password"
              type="password"
              required={false}
              value={password}
              error={errors.password}
              onChange={this.handleChange}
              label="Password"/>
            <p className="text-danger">{ error ? error.message : null }</p>

		        <div className="form-group flexbox">
		          <a className="text-muted hover-primary fs-13" href="#">Forgot password?</a>
		        </div>

		        <div className="form-group">
	            <SmartButton type='submit' title='Sign In' isLoaoding={authenticating} color=' btn-block btn-primary' disabled={false} />
		        </div>
          </form>
        </div>
      </div>
		);
  }
}

// Mapping Store to Component Props
function mapStateToProps  (store){
	return {
    authenticated: store.accounts.authenticated,
    error: store.accounts.authenticateError
  };
}

// Mapping Dispatch to Component Props
function mapDispatchToProps  (dispatch){
	return {
		login: (data) => dispatch(loginAccount(data))
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(Login)
