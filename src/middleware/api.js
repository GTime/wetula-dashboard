/*-- API Middleware --*/
import axios from 'axios';
// const BASE_URL = 'https://wetula-app.herokuapp.com/api';
const BASE_URL = 'http://localhost:3002/api';

export default ({ dispatch }) => next => action => {
  if (action.type !== 'API' ) return next(action);

  const { url, actions, method, data, success } = action.payload;
  const methods = {
    GET: 'get',
    POST: 'post',
    PATCH: 'patch',
    DELETE: 'delete'
  }

  dispatch({ type: actions.initial });
  axios[methods[method]](BASE_URL + url, data)
    .then(response => response.data)
    .then(response => {
      dispatch({
        type: actions.fulfilled,
        payload: response
      });
      if (success) {
        dispatch(success());
      }
    })
    .catch(error => dispatch({
      type: actions.rejected,
      payload: handleError(error.response)
    }));
}

const handleError = err => {
  if (typeof err == 'object' && err.hasOwnProperty('data')) {
    return err.data
  }
  return {
    message: "An error occured"
  }
}
