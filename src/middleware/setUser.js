/*-- API Middleware --*/
import axios from 'axios';
// const BASE_URL = 'https://wetula-app.herokuapp.com/api/v1';
const BASE_URL = 'http://localhost:3002/api/v1';

export default ({ dispatch }) => next => action => {
  if (action.type !== 'API' ) return next(action);

  const { url, actions, method, data, success } = action.payload;
  const methods = {
    GET: 'get',
    POST: 'post',
    PATCH: 'patch',
    DELETE: 'delete'
  }

  dispatch({ type: actions.initial });
  axios[methods[method]](BASE_URL + url, data)
    .then(response => response.data)
    .then(response => {
      // if (actions.initial === 'AUTH_ACCOUNT') {
      //   if (success){
      //     dispatch(success(response));
      //   }
      //   dispatch({
      //     type: actions.fulfilled,
      //     payload: response
      //   });
      // }
      dispatch({
        type: actions.fulfilled,
        payload: response
      });
    })
    .catch(error => dispatch({
      type: actions.rejected,
      payload: error.response
    }));

}
