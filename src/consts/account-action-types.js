import setConstant from '../tools/redux/setConstant';

export const API = 'API';
export const FETCH_ACCOUNTS = setConstant('FETCH_ACCOUNTS');
export const FETCH_ACCOUNT = setConstant('FETCH_ACCOUNT');
export const ADD_ACCOUNT = setConstant('ADD_ACCOUNT');
export const AUTH_ACCOUNT = setConstant('AUTH_ACCOUNT');
export const SET_ACCOUNT = setConstant('SET_ACCOUNT');
export const LOGOUT = 'LOGOUT';
export const UPDATE_ACCOUNT = setConstant('UPDATE_ACCOUNT');
export const CHANGE_ACCOUNT_PASSWORD = setConstant('CHANGE_ACCOUNT_PASSWORD');
export const DISABLE_ACCOUNT = setConstant('DISABLE_ACCOUNT');
export const DELETE_ACCOUNT = setConstant('DELETE_ACCOUNT');
export const SEARCH_ACCOUNT = 'SEARCH_ACCOUNT';
export const RESET_ACCOUNT = 'RESET_ACCOUNT';
