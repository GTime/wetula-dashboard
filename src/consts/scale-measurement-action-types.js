import setConstant from '../tools/redux/setConstant';

export const API = 'API';
export const FETCH_SCALE_MEASUREMENTS = setConstant('FETCH_SCALE_MEASUREMENTS');
export const SEARCH_SCALE_MEASUREMENT = 'SEARCH_SCALE_MEASUREMENT';
