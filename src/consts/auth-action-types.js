import setConstant from '../tools/redux/setConstant';

export const API = 'API';
export const LOGIN = setConstant('FETCH_FARMERS');
export const LOGOUT = 'LOGOUT';
