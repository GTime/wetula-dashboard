import setConstant from '../tools/redux/setConstant';

export const API = 'API';
export const FETCH_FARMERS = setConstant('FETCH_FARMERS');
export const FETCH_FARMER = setConstant('FETCH_FARMER');
export const SEARCH_FARMER = 'SEARCH_FARMER';
