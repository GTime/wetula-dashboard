import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux'

import App from './components/App'
import store from './store'
import setAuthorizationToken from './tools/setAuthorizationToken';
import { getToken } from './tools/token';
import { setUser } from './actions/accounts';
import registerServiceWorker from './registerServiceWorker'

if (localStorage.wetula) {
  setAuthorizationToken(localStorage.authToken);
  store.dispatch(setUser( getToken('wetula') ));
}

ReactDOM.render(
  <Provider store={store}>
    <App/>
  </Provider>,
document.getElementById('root'));
registerServiceWorker();
