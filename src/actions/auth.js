import * as actions from '../consts/auth-action-types';


export const login = (data) => ({
  type: actions.API,
  payload: {
    url: '/auth/logout',
    method: 'POST',
    actions: actions.LOGIN,
    data
  }
});

export const logout = () => ({
  type: actions.LOGOUT,
});
