import axios from 'axios';
import decode from 'jwt-decode'
import setAuthorizationToken from '../tools/setAuthorizationToken'
import { getToken, setToken } from '../tools/token'
import * as actions from '../consts/account-action-types';


export const fetchAccounts = () => ({
  type: actions.API,
  payload: {
    url: '/accounts',
    method: 'GET',
    actions: actions.FETCH_ACCOUNTS
  }
});
export const fetchAccount = (id) => ({
  type: actions.API,
  payload: {
    url: '/accounts/' + id,
    method: 'GET',
    actions: actions.FETCH_ACCOUNT
  }
});
export const addAccount = (data) => ({
  type: actions.API,
  payload: {
    url: '/accounts',
    method: 'POST',
    data,
    actions: actions.ADD_ACCOUNT,
    success: () => fetchAccounts()
  }
});
export const updateAccount = (id, data) => ({
  type: actions.API,
  payload: {
    url: '/accounts/' + id,
    method: 'PATCH',
    data,
    actions: actions.UPDATE_ACCOUNT,
    success: () => fetchAccount(id)
  }
});
export const changeAccountPassword = (id, data) => ({
  type: actions.API,
  payload: {
    url: '/accounts/changePassword/' + id,
    method: 'PATCH',
    data,
    actions: actions.CHANGE_ACCOUNT_PASSWORD,
    success: () => fetchAccount(id)
  }
});
export const disableAccount = (id) => ({
  type: actions.API,
  payload: {
    url: '/accounts/disable/' + id,
    method: 'PATCH',
    actions: actions.DISABLE_ACCOUNT,
    success: () => fetchAccount(id)
  }
});
export const deleteAccount = (id) => ({
  type: actions.API,
  payload: {
    url: '/accounts/' + id,
    method: 'DELETE',
    actions: actions.DELETE_ACCOUNT,
    success: () => fetchAccount(id)
  }
});
export const resetAccount = () => ({
  type: actions.RESET_ACCOUNT
});


export function loginAccount(data){
  // const url = 'https://wetula-app.herokuapp.com/api/v1/accounts/login';
  const url = 'http://localhost:3002/api/accounts/login';
  return (dispatch) => {
    dispatch({
      type: actions.AUTH_ACCOUNT.initial
    });
    axios.post(url, data)
    .then((response) => {
      const data = response.data;
      setToken('wetula', data.token);
      setAuthorizationToken(data.token);
      dispatch( setUser( getToken('wetula')) );
    })
    .catch((err) => {
      dispatch({
        type: actions.AUTH_ACCOUNT.rejected,
        payload: err.response
      })
    })
  }
}
export function isTokenExpired(token) {
  try {
    const decoded = decode(token);
    const now     = new Date();
    const expDate = new Date(decoded.exp * 1000);
    return (expDate < now) ? true : false;
  } catch (e) {
    return false;
  }
}
export function setUser(token){
  const user = decode(token).user;
  if(isTokenExpired(token)){
    logout();
    return {
      type: actions.AUTH_ACCOUNT.rejected,
      payload: { message: 'Token has expired' }
    }
  }
  else {
    return {
      type: actions.AUTH_ACCOUNT.fulfilled,
      payload: user
    }
  }
}
export function logout(){
  return (dispatch) => {
      localStorage.removeItem('wetula');
      setAuthorizationToken(false);
      dispatch({
        type: actions.LOGOUT,
        payload: null
      })
  }
}
