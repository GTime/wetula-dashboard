import * as actions from '../consts/scale-measurement-action-types';

export const fetchScaleMeasurements = () => ({
  type: actions.API,
  payload: {
    url: '/measurements',
    method: 'GET',
    actions: actions.FETCH_SCALE_MEASUREMENTS
  }
});
