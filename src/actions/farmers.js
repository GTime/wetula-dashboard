import * as actions from '../consts/farmer-action-types';


export const fetchFarmers = () => ({
  type: actions.API,
  payload: {
    url: '/farmers',
    method: 'GET',
    actions: actions.FETCH_FARMERS
  }
});
export const fetchFarmer = (id) => ({
  type: actions.API,
  payload: {
    url: '/farmers/' + id,
    method: 'GET',
    actions: actions.FETCH_FARMER
  }
});
