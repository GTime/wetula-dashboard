/*-- Store --*/
import { applyMiddleware, createStore } from 'redux';
import { createLogger } from 'redux-logger';
import thunk from 'redux-thunk';
import promise from 'redux-promise-middleware';
import api from './middleware/api';

// Reducers
import reducers from './reducers/root';

const middleware = applyMiddleware(
  promise(),
  thunk,
  createLogger(),
  api,
);
export default createStore(reducers, middleware);
